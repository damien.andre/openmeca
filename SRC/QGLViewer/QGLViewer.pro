######################################################################
# qmake internal options
######################################################################


CONFIG  += warn_on no_keywords silent release staticlib
QT      +=  xml opengl 
TEMPLATE = lib
TARGET   = ./BUILD/QGLViewer
DESTDIR  = ./
MOC_DIR  = ./BUILD/moc
UI_DIR   = ./BUILD/ui
OBJECTS_DIR = ./BUILD/obj
RCC_DIR     = ./BUILD/rcc
INCLUDEPATH = ./QGLViewer
FORMS      *= ./QGLViewer/VRenderInterface.ui

HEADERS += ./QGLViewer/qglviewer.h \
	  ./QGLViewer/camera.h \
	  ./QGLViewer/manipulatedFrame.h \
	  ./QGLViewer/manipulatedCameraFrame.h \
	  ./QGLViewer/frame.h \
	  ./QGLViewer/constraint.h \
	  ./QGLViewer/keyFrameInterpolator.h \
	  ./QGLViewer/mouseGrabber.h \
	  ./QGLViewer/quaternion.h \
	  ./QGLViewer/vec.h \
	  ./QGLViewer/domUtils.h \
	  ./QGLViewer/config.h \
          ./QGLViewer/VRender/AxisAlignedBox.h \
          ./QGLViewer/VRender/Exporter.h \
          ./QGLViewer/VRender/gpc.h \
          ./QGLViewer/VRender/NVector3.h \
          ./QGLViewer/VRender/Optimizer.h \
          ./QGLViewer/VRender/ParserGL.h \
          ./QGLViewer/VRender/Primitive.h \
          ./QGLViewer/VRender/PrimitivePositioning.h \
          ./QGLViewer/VRender/SortMethod.h \
          ./QGLViewer/VRender/Types.h \
          ./QGLViewer/VRender/Vector2.h \
          ./QGLViewer/VRender/Vector3.h \
          ./QGLViewer/VRender/VRender.h


SOURCES = ./QGLViewer/qglviewer.cpp \
	  ./QGLViewer/camera.cpp \
	  ./QGLViewer/manipulatedFrame.cpp \
	  ./QGLViewer/manipulatedCameraFrame.cpp \
	  ./QGLViewer/frame.cpp \
	  ./QGLViewer/saveSnapshot.cpp \
	  ./QGLViewer/constraint.cpp \
	  ./QGLViewer/keyFrameInterpolator.cpp \
	  ./QGLViewer/mouseGrabber.cpp \
	  ./QGLViewer/quaternion.cpp \
	  ./QGLViewer/vec.cpp \
          ./QGLViewer/VRender/BackFaceCullingOptimizer.cpp \
          ./QGLViewer/VRender/BSPSortMethod.cpp \
          ./QGLViewer/VRender/EPSExporter.cpp \
          ./QGLViewer/VRender/Exporter.cpp \
          ./QGLViewer/VRender/FIGExporter.cpp \
          ./QGLViewer/VRender/gpc.cpp \
          ./QGLViewer/VRender/ParserGL.cpp \
          ./QGLViewer/VRender/Primitive.cpp \
          ./QGLViewer/VRender/PrimitivePositioning.cpp \
          ./QGLViewer/VRender/TopologicalSortMethod.cpp \
          ./QGLViewer/VRender/VisibilityOptimizer.cpp \
          ./QGLViewer/VRender/Vector2.cpp \
          ./QGLViewer/VRender/Vector3.cpp \
          ./QGLViewer/VRender/NVector3.cpp \
          ./QGLViewer/VRender/VRender.cpp




  SOURCES *= \
	

  VRENDER_HEADERS = \
	

target.path = ./BUILD/lib
include.path = ./BUILD/include/QGLViewer
include.files = $${HEADERS}
INSTALLS *= target include
