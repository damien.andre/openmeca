// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Util_MotionLaw_hpp
#define OpenMeca_Util_MotionLaw_hpp

#include "OpenMeca/Util/CustomChFunction.hpp"


namespace OpenMeca
{  
  namespace Util
  {


    class MotionLaw 
    {
    public:
      
      MotionLaw();
      virtual ~MotionLaw();
      
      MotionLaw(const MotionLaw&);           
      MotionLaw& operator=(const MotionLaw&);

      bool IsEnabled() const;

      // Accessors
      OMC_ACCESSOR(Enabled, bool            ,  enabled_);
      OMC_ACCESSOR(Law    , CustomChFunction,  law_    );

    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    private:
      bool enabled_;
      CustomChFunction law_;
    };

    template<class Archive>
    inline void
    MotionLaw::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_NVP(enabled_);
      ar & BOOST_SERIALIZATION_NVP(law_);
    }

  }
}

#endif
