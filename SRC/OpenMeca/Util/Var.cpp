// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Util/Var.hpp"
#include "OpenMeca/Core/System.hpp"


namespace OpenMeca
{  
  namespace Util
  {

    exprtk::symbol_table<double> Var::symbolTable_ = exprtk::symbol_table<double>();
      
    
    exprtk::symbol_table<double>&
    Var::GetSymbolTable()
    {
      return symbolTable_;
    }

    void 
    Var::UpdateTable()
    {
      symbolTable_.clear();

      Core::SetOf<Var>& set = Core::SetOf<Var>::GetGlobalSet();
      for (unsigned int i =0; i < set.GetTotItemNumber(); ++i)
	set(i).AddToTable();

      symbolTable_.add_variable("t", Core::System::Get().GetTime());
      symbolTable_.add_constants();
    }


    Var::Var()
      :symbol_("x"),
       value_(0.)
    {
    }

    Var::Var(const std::string& str, double& val)
      :symbol_(str),
       value_(val)
    {
    }

    Var::~Var()
    {
    }

    void 
    Var::AddToTable()
    {
      OMC_ASSERT_MSG(symbolTable_.add_variable(symbol_, value_),
		     "The symbol \"" + symbol_ + "\" is not valid");
    }


    bool
    Var::IsDoublon()
    {
      Core::SetOf<Var>& set = Core::SetOf<Var>::GetGlobalSet();
      for (unsigned int i =0; i < set.GetTotItemNumber(); ++i)
	if (&set(i) != this)
	  if (set(i).symbol_ == symbol_)
	    return true;
      
      return false;
    }


    bool
    Var::IsValid()
    {
      exprtk::symbol_table<double> table;
      return table.add_variable(symbol_, value_);
    }

    

  }

}


