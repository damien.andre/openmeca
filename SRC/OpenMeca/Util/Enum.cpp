// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <boost/assign.hpp>




#include "ChronoEngine/physics/ChSystem.h"

#include "QGLViewer/camera.h"

#include "OpenMeca/Util/Enum.hpp"
#include "OpenMeca/Physic/PhysEnum.hpp"

namespace OpenMeca
{
  namespace Util
  {

    template<>
    Enum<chrono::ChSystem::eCh_lcpSolver>::Map 
    Enum<chrono::ChSystem::eCh_lcpSolver>::map_ = 
      boost::assign::map_list_of 
      ("SOR"     , chrono::ChSystem::LCP_ITERATIVE_SOR)
      ("SYMMSOR" , chrono::ChSystem::LCP_ITERATIVE_SYMMSOR)
      ("SIMPLEX" , chrono::ChSystem::LCP_SIMPLEX)
      ("JACOBI"  , chrono::ChSystem::LCP_ITERATIVE_JACOBI)
      ("SOR_MULTITHREAD", chrono::ChSystem::LCP_ITERATIVE_SOR_MULTITHREAD)
      ("PMINRES"        , chrono::ChSystem::LCP_ITERATIVE_PMINRES)
      ("BARZILAIBORWEIN", chrono::ChSystem::LCP_ITERATIVE_BARZILAIBORWEIN)
      ("PCG"            , chrono::ChSystem::LCP_ITERATIVE_PCG)
      ("APGD"           , chrono::ChSystem::LCP_ITERATIVE_APGD)
      ("DEM"            , chrono::ChSystem::LCP_DEM);

    template<>
    Enum<chrono::ChSystem::eCh_integrationType>::Map 
    Enum<chrono::ChSystem::eCh_integrationType>::map_ = 
      boost::assign::map_list_of 
      ("ANITESCU", chrono::ChSystem::INT_ANITESCU)
      ("TASORA"  , chrono::ChSystem::INT_TASORA);

    template<>
    Enum<OpenMeca::Physic::MechActionEnum::DirectionMode>::Map 
    Enum<OpenMeca::Physic::MechActionEnum::DirectionMode>::map_ = 
      boost::assign::map_list_of 
      ("ABSOLUTE", OpenMeca::Physic::MechActionEnum::ABSOLUTE_DIR)
      ("BODY"  , OpenMeca::Physic::MechActionEnum::BODY_COORDINATE_DIR);

    template<>
    Enum<OpenMeca::Physic::MechActionEnum::StartPointMode>::Map 
    Enum<OpenMeca::Physic::MechActionEnum::StartPointMode>::map_ = 
      boost::assign::map_list_of 
      ("ABSOLUTE", OpenMeca::Physic::MechActionEnum::ABSOLUTE_POS)
      ("BODY"  , OpenMeca::Physic::MechActionEnum::BODY_COORDINATE_POS);

    template<>
    Enum<qglviewer::Camera::Type>::Map 
    Enum<qglviewer::Camera::Type>::map_ = 
      boost::assign::map_list_of 
      ("PERSPECTIVE" , qglviewer::Camera::PERSPECTIVE)
      ("ORTHOGRAPHIC", qglviewer::Camera::ORTHOGRAPHIC);
    

  }
}
