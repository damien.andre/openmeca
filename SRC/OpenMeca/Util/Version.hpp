// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Util_Version_hpp
#define OpenMeca_Util_Version_hpp

#include <string>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

namespace OpenMeca
{
  namespace Util
  {
    class Version;

    std::ostream& operator<< (std::ostream &, const Version &);

    
    // openmeca's color class
    class Version
    {
    public:
      friend std::ostream& operator<< (std::ostream &, const Version &);

    public:
      Version(const std::string&);
      Version();
      ~Version();

      void ReadString(const std::string&);
      const std::string& ToString() const;

      bool operator<(const Version& otherVersion)  const;
      bool operator>(const Version& otherVersion) const;
      bool operator==(const Version& otherVersion ) const;
      
    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    private:
      std::string str_;
      unsigned int omc_major_, omc_minor_, omc_revision_;
      
    private :
      //Copy constructor and assignment operator are built by compiler
    }; 
    
    template<class Archive>
    inline void
    Version::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_NVP(str_);
      ar & BOOST_SERIALIZATION_NVP(omc_major_);
      ar & BOOST_SERIALIZATION_NVP(omc_minor_);
      ar & BOOST_SERIALIZATION_NVP(omc_revision_);
    }

  }
}
#endif
