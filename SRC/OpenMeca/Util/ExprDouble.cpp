// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <sstream>

#include <boost/lexical_cast.hpp>

#include "OpenMeca/Util/ExprDouble.hpp"
#include "OpenMeca/Util/Var.hpp"




namespace OpenMeca
{  
  namespace Util
  {
      
    ExprDouble::ExprDouble()
      :val_(0.), exp_(val_)
    {
    }

    ExprDouble::ExprDouble(double val)
      :val_(val), exp_(val_)
    {
    }


    ExprDouble::~ExprDouble()
    {
    }


    ExprDouble& 
    ExprDouble::operator=(const double& val)
    {
      val_ = val;
      exp_.SetExpressionFromValue(val);
      return (*this);
    }

    ExprDouble& 
    ExprDouble::operator=(const ExprDouble& val)
    {
      exp_ = val.exp_;
      val_ = val_;
      return (*this);
    }

    void
    ExprDouble::Update()
    {
      exp_.Update();
    }
  
    
    void 
    ExprDouble::SetDimension(const Dimension& dim)
    {
      exp_.SetDimension(dim);
    }
    
    const Dimension& 
    ExprDouble::GetDimension() const
    {
      return exp_.GetDimension();
    }


  }

}


