// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <sstream>

#include <boost/lexical_cast.hpp>

#include "OpenMeca/Util/ExprVector.hpp"
#include "OpenMeca/Util/Var.hpp"




namespace OpenMeca
{  
  namespace Util
  {
      
    ExprVector::ExprVector(std::function<const Geom::Frame<_3D>& ()> f)
      :Geom::Vector<_3D>(f),
       expX_(Geom::Vector<_3D>::operator[](0)),
       expY_(Geom::Vector<_3D>::operator[](1)),
       expZ_(Geom::Vector<_3D>::operator[](2))
    {
      
    }

    ExprVector::ExprVector(double x, double y, double z, std::function<const Geom::Frame<_3D>& ()> f)
      :Geom::Vector<_3D>(x, y, z, f),
       expX_(Geom::Vector<_3D>::operator[](0)),
       expY_(Geom::Vector<_3D>::operator[](1)),
       expZ_(Geom::Vector<_3D>::operator[](2))
    {
      
    }


    ExprVector::~ExprVector()
    {
    }


    ExprVector& 
    ExprVector::operator=(const Geom::Vector<_3D>& p)
    {
      Geom::Vector<_3D>& me = *this;
      me = p;
      expX_.SetExpressionFromValue(me[0]);
      expY_.SetExpressionFromValue(me[1]);
      expZ_.SetExpressionFromValue(me[2]);
      return (*this);
    }

    ExprVector& 
    ExprVector::operator=(const ExprVector& v)
    {
      expX_ = v.expX_;
      expY_ = v.expY_;
      expZ_ = v.expZ_;
      return (*this);
    }

    void 
    ExprVector::SetDimension(const Dimension& dim)
    {
      expX_.SetDimension(dim);
      expY_.SetDimension(dim);
      expZ_.SetDimension(dim);
    }
    
    const Dimension& 
    ExprVector::GetDimension() const
    {
      return expX_.GetDimension();
    }

  

  }

}


