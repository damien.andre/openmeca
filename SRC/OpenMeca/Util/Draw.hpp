// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Util_Draw_hpp
#define OpenMeca_Util_Draw_hpp



namespace OpenMeca
{  
  namespace Util
  {

    // Base class for all 3D drawable items.
    class Draw
    {
    
    public:
      static void Cylinder(float radius, float length, bool middlePositionning=true);
      static void CylinderStripe(float radius, float length);
      static void Cylinder(float externRadius, float interRadius, float length);
      static void Cylinder(float externRadius, float interRadius, float length, float startAngle, float stopAngle);

      static void Sphere(float radius);
      static void PartialSphere(double r,int n,int method,
				double theta1,double theta2,double phi1,double phi2);
      static void Box(float lx, float ly, float lz);
      static void Channel(double Re,double Ri,double l);

      static void Cone(float r, float l);

      static void OuterCylinderTeeth(float Ro, float mo, float alphao, float thickness);
      static void InnerCylinderTeeth(float Ro, float mo, float alphao, float thickness);
      static void StraightTeeth(float mo, float alphao, float thickness, float length);
    };



  }

}




#endif
