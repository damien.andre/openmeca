## This file is part of OpenMeca, an easy software to do mechanical simulation.
##
## Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
##
## Copyright (C) 2012 Damien ANDRE
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

HEADERS += Util/Color.hpp \
           Util/Draw.hpp \
           Util/Icon.hpp \
           Util/Enum.hpp \
           Util/Dimension.hpp \
           Util/Unit.hpp \
           Util/Lang.hpp \
           Util/Version.hpp \
           Util/Var.hpp \
           Util/exprtk.hpp \
           Util/Expr.hpp \
           Util/ExprDouble.cpp \
           Util/ExprPoint.cpp \
           Util/ExprVector.cpp \
           Util/ExprAttitude.cpp \
           Util/MotionLaw.hpp \
           Util/CustomChFunction.hpp 


SOURCES += Util/Color.cpp \
           Util/Draw.cpp \
           Util/Icon.cpp \
           Util/Dimension.cpp \
           Util/Enum.cpp \
           Util/Lang.cpp \
           Util/Unit.cpp \
           Util/Version.cpp \
           Util/Var.cpp \
           Util/Expr.cpp \
           Util/ExprDouble.cpp \
           Util/ExprPoint.cpp \
           Util/ExprVector.cpp \
           Util/ExprAttitude.cpp \
           Util/MotionLaw.cpp \
           Util/CustomChFunction.cpp 

