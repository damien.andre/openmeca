// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_Action_ActionWithoutSelection_hpp
#define OpenMeca_Core_Action_ActionWithoutSelection_hpp


#include <QAction>
#include <typeinfo>

#include "OpenMeca/Core/Action/Action.hpp"
#include "OpenMeca/Core/UserItem.hpp"
#include "OpenMeca/Core/SystemSetting.hpp"
#include "OpenMeca/Core/SelectionManager.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"

namespace OpenMeca
{
  namespace Core
  {

    template <class T, class Do>
    class ActionWithoutSelection : public Action
    {
    public:    
      ActionWithoutSelection();
      virtual ~ActionWithoutSelection();
      void DoAction();

    private:
      ActionWithoutSelection(const ActionWithoutSelection<T,Do>&);                  //Not Allowed    
      ActionWithoutSelection<T,Do>& operator=(const ActionWithoutSelection<T,Do>&); //Not Allowed										            
    };
    
    template<class T, class Do>
    inline 
    ActionWithoutSelection<T,Do>::ActionWithoutSelection()
      :Action(Do::Icon(), Do::Text(), Do::Id())
    {
    }

    template<class T, class Do>
    inline 
    ActionWithoutSelection<T,Do>::~ActionWithoutSelection()
    {
    }
    

    template<class T, class Do>
    inline void 
    ActionWithoutSelection<T,Do>::DoAction()
    {
      Do::DoAction(static_cast<Action&>(*this)); 
    }
    
  }
}

#endif
