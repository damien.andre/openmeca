// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_Action_ActionEditGlobalSetting_hpp
#define OpenMeca_Core_Action_ActionEditGlobalSetting_hpp



#include "OpenMeca/Core/Action/Action.hpp"


namespace OpenMeca
{
  namespace Core
  {
    
    template <class T>
    class ActionEditGlobalSetting
    {
    public:    
      static QString Text();
      static std::string Id();
      static QIcon Icon();
      static void DoAction(Action& action);
    };
    
    template<class T> 
    inline QString 
    ActionEditGlobalSetting<T>::Text()
    {
      return Action::tr("Edit");
    }

    template<class T> 
    inline std::string
    ActionEditGlobalSetting<T>::Id()
    {
      return "Edit";
    }
    
    
    
    template<class T>
    inline QIcon
    ActionEditGlobalSetting<T>::Icon()
    {
      return QIcon(":/Rsc/Img/"+QString(T::GetStrType().c_str()) + "_" + Text() + ".png");
    }
    
    template<class T>
    inline void
    ActionEditGlobalSetting<T>::DoAction(Action&)
    {
      OMC_ASSERT_MSG(0, "TODO");
    }

 
   
  }
}

#endif
