// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_Action_ActionWithSelectedItemT_hpp
#define OpenMeca_Core_Action_ActionWithSelectedItemT_hpp


#include <QAction>
#include <typeinfo>

#include "OpenMeca/Core/Action/ActionWithSelectedItem.hpp"
#include "OpenMeca/Core/Item.hpp"
#include "OpenMeca/Core/SelectionManager.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"

namespace OpenMeca
{
  namespace Core
  {

    template <class T, class Do>
    class ActionWithSelectedItemT : public ActionWithSelectedItem
    {
    public:    
      ActionWithSelectedItemT();
      virtual ~ActionWithSelectedItemT();
      void DoAction();

    protected:

    private:
      ActionWithSelectedItemT(const ActionWithSelectedItemT<T,Do>&);              //Not Allowed    
      ActionWithSelectedItemT<T,Do>& operator=(const ActionWithSelectedItemT<T,Do>&);//Not Allowed
										            
    private:
      void ItemSelected(Core::Item* item);
      T& GetItemSelected();
      bool IsMyType(Core::Item&);
      
    };
    
    template<class T, class Do>
    inline 
    ActionWithSelectedItemT<T,Do>::ActionWithSelectedItemT()
      :ActionWithSelectedItem(Do::Icon(), Do::Text(), Do::Id())
    {
    }

    template<class T, class Do>
    inline 
    ActionWithSelectedItemT<T,Do>::~ActionWithSelectedItemT()
    {
    }
    
    template<class T, class Do>
    inline void
    ActionWithSelectedItemT<T,Do>::ItemSelected(Core::Item* item)
    {
      
      QAction::setEnabled(typeid(T*) == typeid(item));
    }
    
    template<class T, class Do>
    inline T& 
    ActionWithSelectedItemT<T,Do>::GetItemSelected()
    {
      //return SelectionManager::Get().GetSelectedItem<T>();
    }

    template<class T, class Do>
    inline bool 
    ActionWithSelectedItemT<T,Do>::IsMyType(Core::Item& item)
    {
      return (typeid(T)==typeid(item));
    }

    template<class T, class Do>
    inline void 
    ActionWithSelectedItemT<T,Do>::DoAction()
    {
      T& item = static_cast<T&>(Core::Singleton<Core::SelectionManager>::Get().GetItemSelected());
      OMC_ASSERT_MSG(AutoRegister<T>::Exist(item), "The item does not exist");
      Do::DoAction(item, *this);      
	     
    }
    
  }
}

#endif
