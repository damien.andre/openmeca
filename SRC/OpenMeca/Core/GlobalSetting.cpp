// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.



#include "OpenMeca/Core/GlobalSetting.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"

namespace OpenMeca
{
  namespace Core
  {

    GlobalSetting::GlobalSetting(const std::string strType)
      :Item(strType),
       file_(Item::GetStrType())
    {
      //Gui::MainWindow::Get().AddGlobalSetting(*this);
      
    }

    GlobalSetting::~GlobalSetting()
    {
      //Gui::MainWindow::Get().RemoveGlobalSetting(*this);
    }
    
     QFile&
     GlobalSetting::OpenXmlConfigFile()
     {
       return file_.Open();
     }

    void
    GlobalSetting::SaveXmlConfigFile(QDomDocument& doc)
    {
      return file_.SaveXml(doc);
    }

    


  }
}
