// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QFile>
#include <QTextStream>
#include <QString>
#include <QStringList>

#include "OpenMeca/Core/Software.hpp"
#include "OpenMeca/Core/Macro.hpp"

namespace OpenMeca
{

  namespace Core
  {

    Software* Software::me_ = 0;

    Software& 
    Software::Get()
    {
      if (me_==0)
    	me_ = new Software();
      return *me_;
    }


      Software::Software()
      : fileInfo_(":/Info.txt"),
	info_(),
	version_()
    {
      ReadInfo();
      version_ = Util::Version(GetInfoField("Version"));
    }

    
   const Util::Version& 
   Software::Version() const
   {
     return version_;
   }

    

    QString
    Software::HtmlText() const
    {
      QString info;
      std::map<const std::string, std::string>::const_iterator it;

      info += "<table>";
      for (it=info_.begin(); it!=info_.end(); ++it)
	{
	  info +="<tr>";
	  info += "<td><i>" + QObject::tr(it->first.c_str()) + "</i></td>";
	  info += "<td>" + QString(it->second.c_str()) + "</td>";
	  info +="</tr>";
	}
      return info;
    }

    std::string 
    Software::GetInfoField(const std::string& key) const
    {
      OMC_ASSERT_MSG(info_.count(key)==1, "Problem while accessing software info file, the key does not exist");
      return info_.at(key);
    }

    
    void
    Software::ReadInfo()
    {
      QFile file(fileInfo_.c_str());
      if(!file.open(QIODevice::ReadOnly))
	{
	  OMC_ASSERT_MSG(0, "Can't read the sofware file info");
	}
      QTextStream in(&file);

      while(!in.atEnd())
	{
	  QString line = in.readLine();    
	  QStringList fields = line.split(":");
	  OMC_ASSERT_MSG(fields.count()==2, "Problem while reading software info file");
	  fields.first().replace(" ","");
	  const std::string key   = (fields.first()).toStdString();
	  const std::string value = (fields.last()).toStdString();

	  OMC_ASSERT_MSG(info_.count(key)==0, "Problem while reading software info file, the key value is already used");
	  info_[key] = value;
	}
    }


      void 
      Software::FakeTranslate()
      {
	// It's just to force translation of some keywords...

	// Keyword inside xml menumanager file
	QObject::tr("Geometry");
	QObject::tr("Cosmetic");
	QObject::tr("Loading");
	QObject::tr("Plot data...");
	QObject::tr("Save data...");
	QObject::tr("Help");
	QObject::tr("About...");
	QObject::tr("Help...");
	QObject::tr("3D viewer help...");
	
	// Keyword inside the info.txt file
	QObject::tr("Author(s)");
	QObject::tr("Version");
	QObject::tr("Licence");
	QObject::tr("Contact");
	QObject::tr("Project");

	// Example keyword
	QObject::tr("gears");
	QObject::tr("gyroscope");
	QObject::tr("reprap");
	QObject::tr("robot");
	QObject::tr("screw");
	QObject::tr("steering");
	QObject::tr("cranck-shaft");		
      }

  }
}





