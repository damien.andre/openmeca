// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_UserRootItemCommonProperty_hpp
#define OpenMeca_Core_UserRootItemCommonProperty_hpp

#include <QTreeWidgetItem>

#include "OpenMeca/Core/UserItemCommonProperty.hpp"
#include "OpenMeca/Gui/TreeView.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"

namespace OpenMeca
{
  namespace Core
  {

    // Common property for the classes that inherit from the 'UserRootItem' class
    template<class T>
    class UserRootItemCommonProperty : public UserItemCommonProperty<T>, public Singleton< UserRootItemCommonProperty<T> >
    {


      friend class Singleton< UserRootItemCommonProperty<T> >;
    public:
      QTreeWidgetItem& GetRootTreeWidgetItem();


    protected:
      UserRootItemCommonProperty();
      virtual ~UserRootItemCommonProperty();
    
    private:
      QTreeWidgetItem* rootTreeWidgetItem_;

    private:
      UserRootItemCommonProperty(const UserRootItemCommonProperty<T>&);               //Not Allowed    
      UserRootItemCommonProperty<T>& operator=(const UserRootItemCommonProperty<T>&); //Not Allowed
    };


    template<class T>
    inline  
    UserRootItemCommonProperty<T>::UserRootItemCommonProperty()
      :UserItemCommonProperty<T>(),
       rootTreeWidgetItem_(new QTreeWidgetItem(&(Gui::MainWindow::Get().GetTreeView())))
    {
      rootTreeWidgetItem_->setIcon(0,ItemCommonProperty<T>::GetIconSymbol());
      rootTreeWidgetItem_->setText(0,ItemCommonProperty<T>::GetQStrType());
    }
    
    template<class T>
    inline  
    UserRootItemCommonProperty<T>::~UserRootItemCommonProperty()
    {
     
    }
    
    template<class T>
    inline  QTreeWidgetItem& 
    UserRootItemCommonProperty<T>::GetRootTreeWidgetItem()
    {
      return *rootTreeWidgetItem_;
    }
  
  }
}


#endif
