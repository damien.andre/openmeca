// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <iostream>


#include "OpenMeca/Core/Macro.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/ConfigDirectory.hpp"


namespace OpenMeca
{
  namespace Core
  {
    QDir ConfigDirectory::dir_ = QDir();

    const QDir&
    ConfigDirectory::Get()
    {
      dir_ = QDir::home();
      
      if (dir_.exists(GetConfigDirectoryName())==false)
	{
	OMC_ASSERT_MSG(dir_.mkdir(GetConfigDirectoryName())==true, "Can't create the config directory");
	
	dir_.cd(GetConfigDirectoryName());

	const QString msg = QObject::tr("The config file directory of openmeca does not exist, openmeca will build a new config directory \"") + dir_.absolutePath() + "\"";
	OMC_INFO_MSG(msg);
	dir_.cdUp();
	}
	 
      OMC_ASSERT_MSG(dir_.cd(GetConfigDirectoryName())==true, "Can't cd to config directory");
      return dir_;
    }

    QString 
    ConfigDirectory::GetConfigDirectoryName()
    {
      return "openmeca";
    }

    bool
    ConfigDirectory::Delete()
    {
      dir_ = QDir::home();
      if (dir_.exists(GetConfigDirectoryName())==true)
	{
	  dir_.cd(GetConfigDirectoryName());
	  return dir_.removeRecursively();
	}
      return true;
    }


    
    
  }
}
