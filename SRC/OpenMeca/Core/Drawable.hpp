// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_Drawable_hpp
#define OpenMeca_Core_Drawable_hpp


#include "OpenMeca/Core/AutoRegister.hpp"

namespace OpenMeca
{  
  namespace Item
  {  
    class Body;
  }
  
  namespace Core
  {

    // Base class for all 3D drawable items.
    class Drawable : public AutoRegister<Drawable>
    {
    public:
      Drawable();
      virtual ~Drawable();
      virtual void Draw(bool withName = false) = 0;
      virtual void PostDraw() {};
    };



  }

}




#endif
