// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_CommonProperty_hpp
#define OpenMeca_Core_CommonProperty_hpp

#include <map>
#include <QMenu>

#include "OpenMeca/Core/Action/Action.hpp"

namespace OpenMeca
{
  namespace Core
  {
    
    // Base class for common property. A common property resumes all the properties
    // that are shared by miscellaneous classes. It replaces 'static' attributes 
    // for classes.
    class CommonProperty
    {    
    public:
      static CommonProperty& GetClass(const std::string);
      
      Action& GetAction(const std::string);
      QMenu& GetPopUpMenu();

    public:
      static void AddClass(const std::string, CommonProperty&);
      void AddAction(Action&);
      void AddPopUpAction(Action&, const std::string subMenuID = "");
      void AddPopUpSeparator();
      void AddPopUpSubMenu(const std::string& title, const QIcon & icon);
      QMenu& GetSubMenu(const std::string& title);
      

      CommonProperty();
      virtual ~CommonProperty();
      

    private:
      static std::map<const std::string, CommonProperty*> registeredClasses_;
      std::map<const std::string, Action*> registeredActions_;
      std::map<const std::string, Action*> registeredPopUpActions_;
      std::map<const std::string, QMenu*> subMenu_;
      QMenu popUpMenu_;

    private:
      CommonProperty(const CommonProperty&);               //Not Allowed    
      CommonProperty& operator=(const CommonProperty&); //Not Allowed
    };

    

    
  }
}


#endif
