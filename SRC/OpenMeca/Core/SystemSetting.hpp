// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_SystemSetting_hpp
#define OpenMeca_Core_SystemSetting_hpp


#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/Item.hpp"

namespace OpenMeca
{
  namespace Core
  {
    
    // Base class for the system setting classes like simulation settings, scales...
    class SystemSetting : public Item,
			  public AutoRegister<SystemSetting>
    {

    public:
      static const std::string GetStrType();
      static const QString GetQStrType();
      static QTreeWidgetItem& GetRootTreeWidgetItem();
      static void SaveAll(boost::archive::text_oarchive& ar, const unsigned int);
      static void LoadAll(boost::archive::text_iarchive& ar, const unsigned int);
    

    public:    
      SystemSetting(const std::string strType);
      virtual ~SystemSetting();

      virtual void Update();
      virtual void Save(boost::archive::text_oarchive&, const unsigned int) = 0;
      virtual void Load(boost::archive::text_iarchive&, const unsigned int) = 0;

    private:
      SystemSetting();
      SystemSetting(const SystemSetting&);            //Not Allowed    
      SystemSetting& operator=(const SystemSetting&); //Not Allowed


    private:
      static QTreeWidgetItem* rootTreeWidgetItem_;
      static std::map<const std::string, SystemSetting*> instances_;

    private:
      const std::string strChildType_;

    };

  


  

  }
}
 
#endif
