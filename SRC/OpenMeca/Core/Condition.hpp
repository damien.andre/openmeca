// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_Condition_hpp
#define OpenMeca_Core_Condition_hpp

#include <QString>
#include <iostream>
#include <sstream>

namespace OpenMeca
{
  namespace Core
  {

    template<typename T>
    class Condition
    {
    public:
      Condition();
      virtual Condition<T>* Copy() const = 0;
      virtual QString ErrorMessage() const = 0;
      virtual bool Check(const T&) const = 0;
      virtual ~Condition();
    };
    
    template<typename T>
    inline
    Condition<T>::Condition()
    {
    }

    template<typename T>
    inline
    Condition<T>::~Condition()
    {
    }
    
    // ----------------- //
    // STRICTLY SUPERIOR //
    // ----------------- //
    template<typename T>
    class Superior : public Condition<T>
    {
    public:
      Superior(const T& treshold);
      Condition<T>* Copy() const;
      QString ErrorMessage() const;
      bool Check(const T&) const;
      ~Superior();

    private:
      const T treshold_;
      
    };
    
    template<typename T>
    inline
    Superior<T>::Superior(const T& treshold)
      :Condition<T>(),
       treshold_(treshold)
    {
    }

    template<typename T>
    inline Condition<T>* 
    Superior<T>::Copy() const
    {
      return new Superior<T>(treshold_);
    }

    template<typename T>
    inline QString
    Superior<T>::ErrorMessage() const
    {
      QString msg(QObject::tr("Must be superior to "));
      std::ostringstream ss;
      ss << treshold_;
      QString val(ss.str().c_str());
      msg+=val;
      return msg;
    }

    template<typename T>
    inline bool
    Superior<T>::Check(const T& val) const
    {
      return (val >= treshold_);
    }

    template<typename T>
    inline Superior<T>::~Superior()
    {
    }

      
  }
}
#endif

