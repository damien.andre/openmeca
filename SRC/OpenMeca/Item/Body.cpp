// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QSvgRenderer>

#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Item/Part.hpp"
#include "OpenMeca/Item/PartUser.hpp"
#include "OpenMeca/Item/PartUserPoint.hpp"
#include "OpenMeca/Core/UserItemCommonProperty.hpp"
#include "OpenMeca/Core/UserRootItemCommonProperty.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Gui/Viewer.hpp"
#include "OpenMeca/Util/Color.hpp"
#include "OpenMeca/Util/Icon.hpp"


#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Item::Body)

namespace OpenMeca
{  
  namespace Item
  {


    void
    Body::Init()
    {
      Core::Singleton< Core::UserRootItemCommonProperty<Body> >::Get().CreateAction_All();
    }
     
  

      
    Body::Body()
      :Core::UserRootItem(Body::GetStrType(), Core::Singleton< Core::UserRootItemCommonProperty<Body> >::Get().GetRootTreeWidgetItem()),
       chBodyPtr_(0),
       color_(),
       mass_(1.),
       center_(0., 0., 0.), 
       quaternion_(0., 0., 0., 1.),
       frame_(center_, quaternion_),
       massCenter_(0., 0., 0., boost::bind(GetFnPtrCstFrame(&Body::GetFrame), boost::ref(*this))), 
       inertia_(1.,0.,0., 0.,1.,0., 0.,0.,1.),
       fixed_(false),
       showReferenceFrame_(false),
      staticFrictionCoef_(0.6),
      slidingFrictionCoef_(0.6),
      showMassCenter_(false)
    {
    }

    Body::~Body()
    {
      // When deleting the chBodyPtr_ attribute, 
      // chronoengine automatically delete the chBody_ attribute
    }


    void
    Body::Draw()
    {
      if (showReferenceFrame_ || showMassCenter_)
	{
	  GetFrame().UpdateGLMatrix();
	  glPushMatrix();
	  glMultMatrixd(GetFrame().GetGLMatrix());

	  if (showReferenceFrame_)
	    GetFrame().Draw();

	  if (showMassCenter_)
	    {
	      glDisable(GL_LIGHTING);
	      glDisable(GL_DEPTH_TEST);
	      glPointSize(5.0f);
	      glColor3f(1.0f, 0.0f, 0.0f);
	      glBegin(GL_POINTS);
	      glVertex3f(massCenter_[0], massCenter_[1], massCenter_[2]);
	      glEnd();
	      glEnable(GL_DEPTH_TEST);
	      glEnable(GL_LIGHTING);
	    }
	  glPopMatrix();
	}
      
      if (showMassCenter_)
	{
	  const std::string str = "G_" + GetName();
	  Gui::MainWindow::Get().GetViewer().DrawText(str, massCenter_);
	}

    }


    const OpenMeca::Item::Body& 
    Body::GetBody() const
    {
      return *this;
    }

    OpenMeca::Item::Body& 
    Body::GetBody()
    {
      return *this;
    }
    

    void 
    Body::ToggleFix()
    {
      fixed_ = !fixed_;
    }

    const bool& 
    Body::IsReferenceFrameShown() const
    {
      return showReferenceFrame_;
    }

    bool& 
    Body::IsReferenceFrameShown()
    {
      return showReferenceFrame_;
    }

    const bool& 
    Body::IsMassCenterShown() const
    {
      return showMassCenter_;
    }

    bool& 
    Body::IsMassCenterShown()
    {
      return showMassCenter_;
    }

    void 
    Body::ToggleShowReferenceFrame()
    {
      showReferenceFrame_ = !showReferenceFrame_;
    }

    void 
    Body::UpdateIcon()
    {
      Body::DrawIcon(GetIcon(), color_.GetQColor(), fixed_);
    }

    Core::SetOfBase<Part> 
    Body::GetParts()
    {
      Core::SetOfBase<Part> set;
      Core::SetOfBase<Part>::it it;
      Core::SetOf<Part>& partSet = Core::AutoRegister<Part>::GetGlobalSet();
      for (it=partSet.Begin() ; it != partSet.End(); it++ )
	{
	  if (&(*it)->GetBody() == this)
	    set.AddItem(**it);
	}
      return set;
    }

    Core::SetOfBase<Core::UserItem> 
    Body::GetAssociatedSelectedItem()
    {
      Core::SetOfBase<Core::UserItem> itemSet;
      Core::SetOfBase<Part> partSet = GetParts();
      Core::SetOf<Part>::it it;
      for (it=partSet.Begin() ; it != partSet.End(); it++ )
	itemSet.AddItem(**it);

      return itemSet;
    }


    void 
    Body::DrawIcon(QIcon& icon, QColor color, bool fixed)
    {
      // Read svg icon file
      QSvgRenderer* svgRenderer = 0;
      if (fixed)
	svgRenderer = new QSvgRenderer(Util::Icon::ChangeColorInSvgFile(":/Rsc/Img/Body-ground.svg", Qt::red, color));
      else
	svgRenderer = new QSvgRenderer(Util::Icon::ChangeColorInSvgFile(":/Rsc/Img/Body.svg", Qt::red, color));

      // render it on pixmap and translate to icon
      const int size = Gui::MainWindow::Get().GetIconSize();
      QPixmap pixmap(QSize(size,size));
      pixmap.fill(QColor (0, 0, 0, 0));
      QPainter painter(&pixmap);
      svgRenderer->render( &painter);
      icon =  QIcon(pixmap);
      delete svgRenderer;
    }

    chrono::ChSharedBodyPtr& 
    Body::GetChBodyPtr()
    {
      OMC_ASSERT_MSG(chBodyPtr_ != 0, "The required pointer is null");
      return chBodyPtr_;
    }

    const chrono::ChSharedBodyPtr& 
    Body::GetChBodyPtr() const
    {
      OMC_ASSERT_MSG(chBodyPtr_ != 0, "The required pointer is null");
      return chBodyPtr_;
    }
    
    void
    Body::InitChSystem(chrono::ChSystem& chSystem)
    {
      chBodyPtr_ = chrono::ChSharedBodyPtr(new chrono::ChBody());
      chSystem.AddBody(chBodyPtr_);
    }

    void
    Body::BuildChSystem(chrono::ChSystem&)
    {
      OMC_ASSERT_MSG(chBodyPtr_ != 0, "The required pointer is null");
    
      if (GetFixed())
	chBodyPtr_->SetBodyFixed(true);

      chBodyPtr_->SetMass(mass_);
      chBodyPtr_->SetPos(massCenter_.ToChVector());
      chBodyPtr_->SetRot(quaternion_.ToChQuaternion());

      chrono::ChMatrix33<double> inertia = inertia_.ToChMatrix();
      chBodyPtr_->SetInertia(&inertia);

      chBodyPtr_->GetMaterialSurface()->SetSfriction(staticFrictionCoef_); 	
      chBodyPtr_->GetMaterialSurface()->SetKfriction(slidingFrictionCoef_); 	
    }

    void
    Body::UpdateValueFromCh()
    {
      chrono::ChFrame<double>& chFrame = chBodyPtr_->GetFrame_COG_to_abs();
      const chrono::ChVector<double> pos = -massCenter_.GetPositionVector().ToChVector();
      
      const chrono::ChQuaternion<double>& chRot = chFrame.GetRot();
      center_     = chFrame.TransformPointLocalToParent(pos);
      quaternion_ = chRot;

      // massCenter_ = chFrame.GetPos();
      // quaternionMassCenter_ = chRot;
    }


    void 
    Body::InitAndManageState() 
    {
      OMC_ASSERT_MSG(center_.GetStateNumber() <= 1,
		     "Problem with registered state, they must be inferior or equal to one");
      OMC_ASSERT_MSG(quaternion_.GetStateNumber() <= 1,
		     "Problem with registered state, they must be inferior or equal to one");

      if (center_.GetStateNumber() == 1)
	center_.SetAsFirstState();

      if (quaternion_.GetStateNumber() == 1)
	quaternion_.SetAsFirstState();

    }


    void
    Body::SaveState()
    {
      center_.SaveState();
      quaternion_.SaveState();
    }

    void
    Body::ResetState()
    {
      center_.ResetState();
      quaternion_.ResetState();
    }

    void 
    Body::RecoveryState(unsigned int num)
    {
      center_.RecoveryState(num);
      quaternion_.RecoveryState(num);
    }

    chrono::ChVector<> 
    Body::ExpressPointInLocalChFrame(const Geom::Point<_3D>& p) const
    {
      OMC_ASSERT_MSG(p.GetFrame() == frame_,
		     "The point must be expressed in local frame coordinate");
      chrono::ChVector<> chp = p.GetPositionVector().ToChVector() - 
	GetMassCenter().GetPositionVector().ToChVector();
      return chp;
    }

  }

}


namespace OpenMeca
{  
  namespace Core
  {

    QString
    ActionFixBody::Text()
    {
      return Action::tr("Fix/Unfix");
    }

    std::string
    ActionFixBody::Id()
    {
      return "Fix/Unfix";
    }

    QIcon
    ActionFixBody::Icon()
    {
      QIcon icon;
      OpenMeca::Item::Body::DrawIcon(icon, Qt::gray, true);
      return icon;
    }

    void
    ActionFixBody::DoAction(OpenMeca::Item::Body& body, Action& action)
    {
      body.ToggleFix();
      Core::System::Get().Update();
      Core::System::Get().GetHistoric().SystemEdited();
      Gui::MainWindow::Get().AddToHistoric(action);
    }

    QString
    ActionShowRefBody::Text()
    {
      return Action::tr("Show/Unshow local frame");
    }

    std::string
    ActionShowRefBody::Id()
    {
      return "Show/Unshow local frame";
    }


    QIcon
    ActionShowRefBody::Icon()
    {
      return Util::Icon::DrawIconFromSvgFile(":/Rsc/Img/Frame.svg");
    }

    void
    ActionShowRefBody::DoAction(OpenMeca::Item::Body& body, Action& action)
    {
      body.ToggleShowReferenceFrame();
      Core::System::Get().Update();
      Core::System::Get().GetHistoric().SystemEdited();
      Gui::MainWindow::Get().AddToHistoric(action);
    }

    

    template<>
    void 
    UserItemCommonProperty<OpenMeca::Item::Body>::CreateAction_Specialized() 
    {
      Action& fixAction = 
	*new ActionWithSelectedItemT<OpenMeca::Item::Body, ActionFixBody >();
      CommonProperty::AddAction(fixAction);
      CommonProperty::AddPopUpAction(fixAction);

      Action& showRefAction = 
	*new ActionWithSelectedItemT<OpenMeca::Item::Body, ActionShowRefBody >();
      CommonProperty::AddAction(showRefAction);
      CommonProperty::AddPopUpAction(showRefAction);

      AddPopUpSeparator();
      
      // don't forget to instanciate 
      Core::Singleton< Core::UserItemCommonProperty<OpenMeca::Item::PartUserPoint> >::Instanciate();
      CreateAction_NewWithSelection<OpenMeca::Item::PartUserPoint>();
    }


    



  }
}
