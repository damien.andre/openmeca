// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.




#include "OpenMeca/Item/How/GetLinPos.hpp"
#include "OpenMeca/Item/Body.hpp"

#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(OpenMeca::Item::How::GetLinPos::MySensor)




namespace OpenMeca
{
  namespace Item
  {
    namespace How
    {
      
      const std::string 
      GetLinPos::GetStrType()
      {
	return Physic::LinearPosition::GetStrType();
      }

      const QString
      GetLinPos::GetQStrType()
      {
	return Physic::LinearPosition::GetQStrType();
      }

      Geom::Vector<_3D> 
      GetLinPos::Acquire(MySensor& sensor)
      {

	PartPoint& parent =  sensor.GetParent();
	Geom::Point<_3D> p(parent.GetPoint(), &Geom::Frame<_3D>::GetGlobal);
	return p.GetPositionVector();
      }

    }
  }
}
    
   

    

