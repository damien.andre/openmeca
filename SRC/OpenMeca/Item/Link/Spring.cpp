 // This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/Link/Spring.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/Drawable.hpp"
#include "OpenMeca/Core/System.hpp"

#include "OpenMeca/Core/Drawable.hpp" 
#include "OpenMeca/Item/PartPoint.hpp" 
#include "OpenMeca/Util/Draw.hpp"
#include "OpenMeca/Item/LinkT.hpp" 




namespace OpenMeca
{  
  namespace Item
  {
    const std::string 
    Spring::GetStrType()
    {
      return "Spring";
    }

    const QString 
    Spring::GetQStrType()
    {
      return QObject::tr("Spring");
    }

    Spring::Spring(Link& link)
      :LinkTypeBase(link),
       stiffness_(0.),
       dampingFactor_(0.),
       freeLength_(0.),
       initialLength_(0.)
    {
    }

    Spring::~Spring()
    {
    }

    
    void 
    Spring::BeginDraw1()
    {
      const Geom::Vector<_3D> axis_glob(GetStartPoint<1>(), GetStartPoint<2>());
      const Geom::Vector<_3D> axis_loc (axis_glob.Unit(), GetLink().GetFrameFctBody<1>());
      const Geom::Vector<_3D>& z = GetLink().GetFrameBody<1>().GetZAxis();
      GetLink().GetPart1().GetQuaternion().SetVecFromTo(z, axis_loc);
      Geom::Point<_3D> p3(GetStartPoint<2>(), GetLink().GetPart1().GetFrameFct());
    }
    


    template<> 
    void 
    Spring::DrawPart<1>()
    {
      
      const double scale = Part::GetScaleValue();
      
      // Draw helicoid
      const float length     = GetCurrentLength();
      const int loopNumber   = 10;
      const int step         = 200.0;
      const float stepLength = length/float(step);
      const float radius      = 0.2f*scale;

      glDisable(GL_LIGHTING);
      glColor3f(0.0, 0.0, 0.0);
      glLineWidth(1.0f);
      glBegin(GL_LINE_STRIP);
      glVertex3f(0.f, 0.f, 0.f);
      for (float l=0; l < length; l = l + stepLength)
	{
	  const float alpha = (2*M_PI*l/length)*float(loopNumber);
	  glVertex3f(radius*cos(alpha), radius*sin(alpha), l);
	}
      glVertex3f(radius, 0.0f, length);
      glVertex3f(0.f, 0.f, length);
      glEnd();
      glEnable(GL_LIGHTING);
    }

    template<> 
    void 
    Spring::DrawPart<2>()
    {
    }


    void 
    Spring::UpdatePart()
    {
      GetLink().GetPart1().GetCenter()[2] = -initialLength_/2.;
      GetLink().GetPart2().GetCenter()[2] =  initialLength_/2.;

    }

    double 
    Spring::GetCurrentLength()
    {
      return Geom::Vector<_3D>(GetStartPoint<1>(), GetStartPoint<2>()).GetNorm();
    }

  }
}




