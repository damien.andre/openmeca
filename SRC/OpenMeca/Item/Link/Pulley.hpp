// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Link_Pulley_hpp
#define OpenMeca_Item_Link_Pulley_hpp

#include <string>
#include <QIcon>
#include <QColor>

#include "OpenMeca/Item/PartPoint.hpp"
#include "OpenMeca/Item/Link/LinkTypeBase.hpp"
#include "OpenMeca/Gui/Dialog/DialogLinkPulley.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Item/Link_CreateAction_SpecializedT.hpp"

#include "ChronoEngine/physics/ChLinkLock.h"
#include "ChronoEngine/physics/ChLinkLock.h"

namespace OpenMeca
{  
  namespace Item
  {

    class Pulley : public LinkTypeBase
    {

    public:
      static const std::string GetStrType();
      static const QString GetQStrType();

      typedef chrono::ChLinkPulley ChLink; 
      typedef Gui::DialogLinkPulley Dialog;


    public:
      Pulley(Link&);
      ~Pulley();

      void UpdatePart();

      template <int N> void DrawPart();
      template <int N> void BuildPoints(Core::SetOf<PartPoint>&, Core::DrawableUserItem&);
      template <int N> chrono::ChFrame<> ComputeLocalShaft();
      template <int N> double ComputePinionBeltAngle();
      template <int N> double ComputeRotationAngle();

      double ComputeRackLength();

      // Accessors
      OMC_ACCESSOR(Radius1        , double ,  radius1_        );
      OMC_ACCESSOR(Radius2        , double ,  radius2_        );
      OMC_ACCESSOR(InterAxisLength, double ,  interAxisLength_);


    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    private:
      double radius1_;
      double radius2_;
      double interAxisLength_;
      double modulus_;
    };

    template<class Archive>
    inline void
    Pulley::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(LinkTypeBase);
      ar & BOOST_SERIALIZATION_NVP(radius1_);
      ar & BOOST_SERIALIZATION_NVP(radius2_);
      ar & BOOST_SERIALIZATION_NVP(interAxisLength_);
    }


    template<int N>
    void 
    Pulley::BuildPoints(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      p1->GetName() = "center";
      p1->SetScaleCoordinate(0., 0., 0.);
      set.AddItem(*p1);
    }

    template<int N>
    chrono::ChFrame<>
    Pulley::ComputeLocalShaft()
    {
      const Geom::Point<_3D> p(GetLink().GetPart<N>().GetCenter(), 
			       GetLink().GetBody<N>().GetFrameFct());
      
      const Geom::Quaternion<_3D> q(GetLink().GetPart<N>().GetQuaternion(), 
				    GetLink().GetBody<N>().GetFrameFct());
      
      const Geom::Frame<_3D> f(p, q);

      return chrono::ChFrame<>(f.ToChCoordsys());
    }

  }
}

namespace OpenMeca
{  
  namespace Core
  {
    template<> 
    inline void
    ItemCommonProperty< OpenMeca::Item::LinkT<OpenMeca::Item::Pulley> >::BuildIconSymbol()
    {
      OpenMeca::Item::LinkT<OpenMeca::Item::Pulley>::BuildIconSymbol(iconSymbol_);
    }

    template<>
    inline void
    UserItemCommonProperty<OpenMeca::Item::LinkT<OpenMeca::Item::Pulley> >::CreateAction_Specialized() 
    {
      OpenMeca::Item::Link_CreateAction_SpecializedT(*this);
    }


  }
}


#endif
