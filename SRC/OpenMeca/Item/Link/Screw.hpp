// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Link_Screw_hpp
#define OpenMeca_Item_Link_Screw_hpp

#include <string>
#include <QIcon>
#include <QColor>

#include "OpenMeca/Item/Link/LinkTypeBase.hpp"
#include "OpenMeca/Gui/Dialog/DialogLinkScrew.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Item/Link_CreateAction_SpecializedT.hpp"

namespace OpenMeca
{    
  namespace Item
  {
   

    struct Screw : public LinkTypeBase
    {
    public:
      static const std::string GetStrType();
      static const QString GetQStrType();

      typedef chrono::ChLinkScrew ChLink; 
      typedef Gui::DialogLinkScrew Dialog;

    public:
      Screw(Link&);
      ~Screw();

      template <int N> void DrawPart();
      template <int N> void BuildPoints(Core::SetOf<PartPoint>&, Core::DrawableUserItem&);

      // Accessors
      OMC_ACCESSOR(Tau , double,  tau_);

    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    private:
      double tau_;

    };


    template<class Archive>
    inline void
    Screw::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(LinkTypeBase);
      ar & BOOST_SERIALIZATION_NVP(tau_);
    }


  }

}


namespace OpenMeca
{  
  namespace Core
  {
    
    template<> 
    inline void
    ItemCommonProperty< OpenMeca::Item::LinkT<OpenMeca::Item::Screw> >::BuildIconSymbol()
    {
      OpenMeca::Item::LinkT<OpenMeca::Item::Screw>::BuildIconSymbol(iconSymbol_);
    }

    template<>
    inline void
    UserItemCommonProperty<OpenMeca::Item::LinkT<OpenMeca::Item::Screw> >::CreateAction_Specialized() 
    {
      OpenMeca::Item::Link_CreateAction_SpecializedT(*this);
    }


  }
}

#endif
