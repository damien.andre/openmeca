// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Link_Spring_hpp
#define OpenMeca_Item_Link_Spring_hpp

#include <string>
#include <QIcon>
#include <QColor>

#include "OpenMeca/Item/PartPoint.hpp"
#include "OpenMeca/Item/Link/LinkTypeBase.hpp"
#include "OpenMeca/Gui/Dialog/DialogLinkSpring.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Item/Link_CreateAction_SpecializedT.hpp"

#include "ChronoEngine/physics/ChLinkLock.h"
#include "ChronoEngine/physics/ChLinkLock.h"

namespace OpenMeca
{  
  namespace Item
  {

    class Spring : public LinkTypeBase
    {

    public:
      static const std::string GetStrType();
      static const QString GetQStrType();

      typedef chrono::ChLinkSpring ChLink; 
      typedef Gui::DialogLinkSpring Dialog;


    public:
      Spring(Link&);
      ~Spring();

      void UpdatePart();
      double GetCurrentLength();

      template <int N> void DrawPart();
      template <int N> void BuildPoints(Core::SetOf<PartPoint>&, Core::DrawableUserItem&);
      template <int N> Geom::Point<_3D> GetStartPoint() const;

      void BeginDraw1();

      // Accessors
      OMC_ACCESSOR(Stiffness      , double ,  stiffness_    );
      OMC_ACCESSOR(DampingFactor  , double ,  dampingFactor_);
      OMC_ACCESSOR(FreeLength     , double ,  freeLength_   );
      OMC_ACCESSOR(InitialLength  , double ,  initialLength_);

      

    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    private:
      double stiffness_;
      double dampingFactor_;
      double freeLength_;
      double initialLength_;
    };

    template<class Archive>
    inline void
    Spring::serialize(Archive& ar, const unsigned int version)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(LinkTypeBase);
      ar & BOOST_SERIALIZATION_NVP(stiffness_);
      ar & BOOST_SERIALIZATION_NVP(dampingFactor_);
      ar & BOOST_SERIALIZATION_NVP(freeLength_);
      if (version > 0)
	{
	  ar & BOOST_SERIALIZATION_NVP(initialLength_);
	}
    }

    template<int N>
    Geom::Point<_3D>
    Spring::GetStartPoint() const
    {
      const Geom::Point<_3D> p(0., 0., 0., GetLink().GetPart<N>().GetFrameFct());
      return p.ToGlobalFrame();
    }

    template<int N>
    void 
    Spring::BuildPoints(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      p1->GetName() = "center";
      p1->SetScaleCoordinate(0., 0., 0.);
      set.AddItem(*p1);
    }


  }
}

namespace OpenMeca
{  
  namespace Core
  {
    
    template<> 
    inline void
    ItemCommonProperty< OpenMeca::Item::LinkT<OpenMeca::Item::Spring> >::BuildIconSymbol()
    {
      OpenMeca::Item::LinkT<OpenMeca::Item::Spring>::BuildIconSymbol(iconSymbol_);
    }

    template<>
    inline void
    UserItemCommonProperty<OpenMeca::Item::LinkT<OpenMeca::Item::Spring> >::CreateAction_Specialized() 
    {
      OpenMeca::Item::Link_CreateAction_SpecializedT(*this);
    }


  }
}

#include <boost/serialization/version.hpp>
BOOST_CLASS_VERSION(OpenMeca::Item::Spring, 1)

#endif
