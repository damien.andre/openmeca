// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/Link/LinearMotor.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Util/Draw.hpp"
#include "OpenMeca/Core/System.hpp"

#include "OpenMeca/Core/Drawable.hpp" 
#include "OpenMeca/Item/PartPoint.hpp" 
#include "OpenMeca/Item/LinkT.hpp" 

#include "OpenMeca/Item/LinkT.hpp" 

namespace OpenMeca
{  
  namespace Item
  {
    const std::string 
    LinearMotor::GetStrType()
    {
      return "LinearMotor";
    }

    const QString 
    LinearMotor::GetQStrType()
    {
      return QObject::tr("LinearMotor");
    }

    bool
    LinearMotor::EnableMotionLaw()
    {
      return false;
    }



    LinearMotor::LinearMotor(Link& link)
      :LinkTypeBase(link),
       displacement_(Core::System::Get().GetTime(), 0.)
    {
      displacement_.SetDimension(Util::Dimension::Get("Length"));
    }
    
    LinearMotor::~LinearMotor()
    {
    }


    template<> 
    void 
    LinearMotor::DrawPart<1>()
    {
      const double scale = Part::GetScaleValue();
      float Lx = 0.2f*scale;
      float Ly = 0.2f*scale;
      float Lz = 0.6f*scale;
      Util::Draw::Box(Lx,Ly,Lz);
      
      Lx = Lx/2;
      Ly = Ly/2;
      Lz = Lz/2;
      glColor3f(0.0f, 0.0f, 0.0f);
      
      glDisable(GL_LIGHTING);
      glLineWidth(3.0f);
      glBegin(GL_LINES);
      glVertex3f(Lx,Ly,Lz);
      glVertex3f(-Lx,-Ly,Lz);
      glVertex3f(Lx,-Ly,Lz);
      glVertex3f(-Lx,Ly,Lz);
      glVertex3f(Lx,Ly,-Lz);
      glVertex3f(-Lx,-Ly,-Lz);
      glVertex3f(Lx,-Ly,-Lz);
      glVertex3f(-Lx,Ly,-Lz);
      glEnd();
      
      glColor3f(1.0f, 1.0f, 1.0f);
      const float s = 0.01f*scale;
      Lx += s;
      Ly *= 2./3.;
      Lz *= 1./4.;
      glBegin(GL_LINE_STRIP);
      glVertex3f(Lx+s, -Ly, -Lz);
      glVertex3f(Lx+s,  Ly, -Lz);
      glVertex3f(Lx+s,  0.,  0.);
      glVertex3f(Lx+s,  Ly,  Lz);
      glVertex3f(Lx+s, -Ly,  Lz);
      glEnd();
      glEnable(GL_LIGHTING);
    }

    

    template<>
    void 
    LinearMotor::BuildPoints<1>(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      p1->GetName() = "middle";
      p1->SetScaleCoordinate(0., 0., 0.);
      set.AddItem(*p1);
    }


    template<> 
    void 
    LinearMotor::DrawPart<2>()
    {
      const double scale = Part::GetScaleValue();
      glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
      Util::Draw::Cylinder(0.01f*scale, 1.0f*scale);
    }


    template<>
    void 
    LinearMotor::BuildPoints<2>(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      PartPoint* p2 = new PartPoint(item);
      PartPoint* p3 = new PartPoint(item);
      p1->GetName() = "left";
      p1->SetScaleCoordinate(0., 0., -0.5);
      p2->GetName() = "middle";
      p2->SetScaleCoordinate(0., 0., 0.);
      p3->GetName() = "right";
      p3->SetScaleCoordinate(0., 0., 0.5);
      set.AddItem(*p1);
      set.AddItem(*p2);
      set.AddItem(*p3);
    }


  }
}




