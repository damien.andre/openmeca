// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/Link/RackPinion.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/Drawable.hpp"
#include "OpenMeca/Core/System.hpp"

#include "OpenMeca/Core/Drawable.hpp" 
#include "OpenMeca/Item/PartPoint.hpp" 
#include "OpenMeca/Util/Draw.hpp"
#include "OpenMeca/Item/LinkT.hpp" 




namespace OpenMeca
{  
  namespace Item
  {
    const std::string 
    RackPinion::GetStrType()
    {
      return "RackPinion";
    }

    const QString 
    RackPinion::GetQStrType()
    {
      return QObject::tr("RackPinion");
    }


    RackPinion::RackPinion(Link& link)
      :LinkTypeBase(link),
       ratio_(1.),
       pinionRadius_(0.),
       modulus_(0.001),
       angleOfAction_(M_PI*20./180.)
    {
    }

    RackPinion::~RackPinion()
    {
    }


    template<>
    void 
    RackPinion::BuildPoints<1>(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      p1->GetName() = "pinion center";
      p1->SetScaleCoordinate(0., 0., 0.);
      set.AddItem(*p1);
    }

    template<> 
    void 
    RackPinion::DrawPart<1>()
    {
      const double scale = Part::GetScaleValue();
      const float l = 0.1f*scale;
      const float R0 = pinionRadius_;
      const float Rb = R0*cos(angleOfAction_); 
      
      glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
      glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
      Util::Draw::Cylinder(Rb*1.01, l);
      glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
      Util::Draw::OuterCylinderTeeth(R0, modulus_, angleOfAction_, l);

    }

    template<>
    void 
    RackPinion::BuildPoints<2>(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      p1->GetName() = "pinion center";
      p1->SetScaleCoordinate(0., 0.1f, 0.);
      set.AddItem(*p1);
    }

 

    template<> 
    void 
    RackPinion::DrawPart<2>()
    {
      const double scale = Part::GetScaleValue();
      glTranslatef(.0f, -modulus_, .0f);
      Util::Draw::StraightTeeth(modulus_, angleOfAction_, 0.1f*scale, 2.0f*scale);
      
      glTranslatef(.0f, 0.1f*scale/2., .0f);
      Util::Draw::Box(2.0f*scale, 0.1f*scale, 0.1f*scale);

    }


    void 
    RackPinion::UpdatePart()
    {
      GetLink().GetPart2().GetCenter()[1] = pinionRadius_ + modulus_;
      
    }


    


  }
}




