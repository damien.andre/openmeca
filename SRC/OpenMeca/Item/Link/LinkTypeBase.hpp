// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Link_LinkTypeBase_hpp
#define OpenMeca_Item_Link_LinkTypeBase_hpp

#include <string>
#include <QIcon>
#include <QColor>

#include "OpenMeca/Core/SetOf.hpp"
#include "OpenMeca/Gui/Dialog/DialogLinkT.hpp"
#include "ChronoEngine/physics/ChLinkLock.h"

namespace OpenMeca
{  
  namespace Core
  { 
    class DrawableUserItem;
  } 
}

namespace OpenMeca
{  
  namespace Item
  {
    class PartPoint;
    class Link;
    template <class T> class LinkT;
    

    class LinkTypeBase
    {
    public:
      LinkTypeBase(Link&);
      virtual ~LinkTypeBase();

      virtual void UpdatePart();

      virtual void BeginDraw1();
      virtual void BeginDraw2();
      
      Link& GetLink();
      const Link& GetLink() const;

    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    private:
      Link& link_;
    };


    template<class Archive>
    inline void
    LinkTypeBase::serialize(Archive&, const unsigned int)
    {
    }


  }

}




#endif
