// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Body_hpp
#define OpenMeca_Item_Body_hpp

#include "OpenMeca/Core/UserRootItem.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"

#include "OpenMeca/Gui/Dialog/DialogBody.hpp"
#include "OpenMeca/Util/Color.hpp"

#include "OpenMeca/Geom/Point.hpp"
#include "OpenMeca/Geom/Quaternion.hpp"
#include "OpenMeca/Geom/Frame.hpp"
#include "OpenMeca/Geom/Matrix.hpp"

#include "ChronoEngine/physics/ChBody.h"


namespace OpenMeca
{  
  namespace Core
  {
    class Action;
  }
}



namespace OpenMeca
{  
  namespace Item
  {

    class Part;
    class PartUser;

    // The physical rigid body
    class Body: public Core::UserRootItem, public Core::AutoRegister<Body>
    {
    
    public:
      static const std::string GetStrType() {return "Body";}; 
      static const QString GetQStrType() {return QObject::tr("Body");}; 
      static void Init();

      friend class  Gui::DialogBody;
      typedef Gui::DialogBody Dialog;

      typedef  const Geom::Frame<_3D>&(Body::*GetFnPtrCstFrame)() const;   

      static void DrawIcon(QIcon&, QColor, bool fixed);
      
    public:
      Body();
      ~Body();

      void Draw();
    

      const OpenMeca::Item::Body& GetBody() const;
      OpenMeca::Item::Body& GetBody();

      void ToggleFix();

      const bool& IsReferenceFrameShown() const;
      bool& IsReferenceFrameShown();
      const bool& IsMassCenterShown() const;
      bool& IsMassCenterShown();
      void ToggleShowReferenceFrame();
      
      Core::SetOfBase<Part> GetParts();

      template<class T> Core::SetOfBase<T> GetChildSet();

      void InitAndManageState();
      void SaveState();
      void ResetState();
      void RecoveryState(unsigned int);

      // Chrono::Engine management
      void InitChSystem(chrono::ChSystem&);
      void BuildChSystem(chrono::ChSystem&);
      void UpdateValueFromCh();

      chrono::ChVector<> ExpressPointInLocalChFrame(const Geom::Point<_3D>&) const; 

      //Accessor
      OMC_ACCESSOR    (Mass           , double                 , mass_           );
      OMC_ACCESSOR    (Inertia        , Geom::Matrix<_3D>      , inertia_        );
      OMC_ACCESSOR    (Quaternion     , Geom::Quaternion<_3D>  , quaternion_     );
      OMC_ACCESSOR    (Frame          , Geom::Frame<_3D>       , frame_          );
      OMC_ACCESSOR    (MassCenter     , Util::ExprPoint        , massCenter_     );
      OMC_ACCESSOR    (Color          , Util::Color            , color_     );
      OMC_ACCESSOR    (Fixed          , bool                   , fixed_     );
      OMC_ACCESSOR    (StaticFrictionCoef , double, staticFrictionCoef_ );
      OMC_ACCESSOR    (SlidingFrictionCoef, double, slidingFrictionCoef_);

      chrono::ChSharedBodyPtr& GetChBodyPtr();
      const chrono::ChSharedBodyPtr& GetChBodyPtr() const;

    private:
      Body(const Body&);             //Not Allowed
      Body& operator=(const Body&);  //Not Allowed

      void UpdateIcon();
      Core::SetOfBase<Core::UserItem> GetAssociatedSelectedItem();
      
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int);

    private:
      chrono::ChSharedBodyPtr chBodyPtr_;
      Util::Color color_;
      double mass_;
      Geom::Point<_3D> center_;
      Geom::Quaternion<_3D> quaternion_;
      Geom::Frame<_3D> frame_;
      Util::ExprPoint massCenter_;
      Geom::Matrix<_3D> inertia_;
      bool fixed_;
      bool showReferenceFrame_;  
      double staticFrictionCoef_;
      double slidingFrictionCoef_;
      bool showMassCenter_;  

    };


    template<class Archive>
    inline void
    Body::serialize(Archive & ar, const unsigned int version)
      {
	ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Core::UserRootItem);
	ar & BOOST_SERIALIZATION_NVP(color_);
	ar & BOOST_SERIALIZATION_NVP(mass_);
	if (version >= 2)
	  ar & BOOST_SERIALIZATION_NVP(massCenter_);
	else
	  {
	    Geom::Point<_3D> p(boost::bind(GetFnPtrCstFrame(&Body::GetFrame), boost::ref(*this)));
	    ar & BOOST_SERIALIZATION_NVP(p);
	    massCenter_ = p;
	  }
	ar & BOOST_SERIALIZATION_NVP(quaternion_);
	ar & BOOST_SERIALIZATION_NVP(inertia_);
	ar & BOOST_SERIALIZATION_NVP(fixed_);
	ar & BOOST_SERIALIZATION_NVP(showReferenceFrame_);
	if (version >= 1)
	  {
	    ar & BOOST_SERIALIZATION_NVP(center_);
	    ar & BOOST_SERIALIZATION_NVP(staticFrictionCoef_);
	    ar & BOOST_SERIALIZATION_NVP(slidingFrictionCoef_);
	  }
	if (version >= 3)
	  {
	    ar & BOOST_SERIALIZATION_NVP(showMassCenter_);
	  }
      }

    template<class T>
    inline Core::SetOfBase<T>
    Body::GetChildSet()
    {
      Core::SetOfBase<UserItem> all;
      UserItem::ConcatenateDependentItems(all);
      Core::SetOf<UserItem>::it it;
      Core::SetOfBase<T> childs;
      for (it=all.Begin() ; it != all.End(); it++ )
	{
	  T* ptr = 0;
	  ptr = dynamic_cast<T*>(*it);
	  if (ptr != 0 && &ptr->GetBody() == this)
	    childs.AddItem(*ptr);
	}
      return childs;
    }


  }

}


namespace OpenMeca
{  
  namespace Core
  {

    template<>
    inline void
    ItemCommonProperty<OpenMeca::Item::Body>::BuildIconSymbol()
    {
      QIcon icon;
      OpenMeca::Item::Body::DrawIcon(iconSymbol_, Qt::gray, false);
    }


    class ActionFixBody
    {
    public:
      static QString Text();
      static std::string Id();
      static QIcon Icon();
      static void DoAction(OpenMeca::Item::Body&, Action& action);
    };

    class ActionShowRefBody
    {
    public:
      static QString Text();
      static std::string Id();
      static QIcon Icon();
      static void DoAction(OpenMeca::Item::Body&, Action& action);
    };

  }
}

#include <boost/serialization/version.hpp>
BOOST_CLASS_VERSION(OpenMeca::Item::Body, 3)

#endif
