// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef _OpenMeca_Item_SensorT_hpp_
#define _OpenMeca_Item_SensorT_hpp_


#include "OpenMeca/Item/Sensor.hpp"
#include "OpenMeca/Core/UserItemCommonProperty.hpp"
#include "OpenMeca/Util/Icon.hpp"
#include "OpenMeca/Gui/Dialog/DialogSensorT.hpp"

namespace OpenMeca
{
  namespace Item
  {

   

    template<class Parent, class Quantity, class How>
    class SensorT : public Sensor, 
                    public Core::AutoRegister<SensorT<Parent, Quantity, How> >
    {
    public:  
      static const std::string GetStrType(); 
      static const QString GetQStrType(); 
      static void Init();
      static void DrawIcon(QIcon&, QColor);
      typedef Gui::DialogSensorT<Parent, Quantity, How> Dialog;

    public:      
      SensorT(Parent&);
      ~SensorT();

      void UpdateIcon();
      void DrawShape();
      void BeginDraw();
      void EndDraw();
      Physic::Quantity& GetQuantity();
      const Physic::Quantity& GetQuantity() const;

      void Acquire();

      void SaveState();
      void ResetState();
      void RecoveryState(unsigned int);
      
      virtual const Geom::Frame<_3D>& GetFrame() const;     

      const Core::AutoRegisteredPtr<Parent, SensorT >& GetParentItemPtr() const;
      Core::AutoRegisteredPtr<Parent, SensorT >& GetParentItemPtr();

      Parent& GetParent();
      const Parent& GetParent() const;

    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    private:
      Core::AutoRegisteredPtr<Parent, SensorT > parentItem_;
      Quantity quantity_;
    };

    template<class Parent, class Quantity, class How>
    template<class Archive>
    inline void
    SensorT<Parent, Quantity, How>::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Sensor);
    }

    
    template<class Parent, class Quantity, class How>
    inline const std::string 
    SensorT<Parent, Quantity, How>::GetStrType()
    {
      return Sensor::GetStrType()+ "@" + Quantity::GetStrType();
    }

    template<class Parent, class Quantity, class How>
    inline const QString
    SensorT<Parent, Quantity, How>::GetQStrType()
    {
      return Sensor::GetQStrType()+ "@" + Quantity::GetQStrType();
    }


    template<class Parent, class Quantity, class How>
    inline void
    SensorT<Parent, Quantity, How>::Init()
    {
      Core::UserItemCommonProperty< SensorT<Parent, Quantity, How> > & prop = 
	Core::Singleton< Core::UserItemCommonProperty< SensorT<Parent, Quantity, How> > >::Get();

      prop.CreateAction_Edit();
      prop.CreateAction_Delete();

      // don't forget to instanciate UserRootItemCommonProperty<Body> before calling
      // create action
      Core::Singleton< Core::UserItemCommonProperty<Parent> >::Instanciate();
      prop.template CreateAction_NewWithAutomaticSelection<Parent>();
    }
    
    template <class Parent, class Quantity, class How>
    inline void 
    SensorT<Parent, Quantity, How>::DrawIcon(QIcon& icon, QColor color)
    {
      // Read svg icon file
      QString svgFileName = ":/Rsc/Img/Sensor/" + QString(Quantity::GetStrType().c_str()) + ".svg";
      Util::Icon::DrawIconFromSvgFile(svgFileName, icon, color);
    }


    template<class Parent, class Quantity, class How> 
    inline 
    SensorT<Parent, Quantity, How>::SensorT(Parent& parent)
      :Sensor(GetStrType(), parent.GetMainTreeItem()), 
       parentItem_(*this, parent),
       quantity_(*this)
    {
      UpdateIcon();
      Sensor::treeItem_->Update();
    }

    template<class Parent, class Quantity, class How> 
    inline 
    SensorT<Parent, Quantity, How>::~SensorT()
    {
    }

    template<class Parent, class Quantity, class How> 
    inline void 
    SensorT<Parent, Quantity, How>::UpdateIcon()
    {
      DrawIcon(GetIcon(), GetColor().GetQColor());
    }

    template<class Parent, class Quantity, class How> 
    inline Physic::Quantity& 
    SensorT<Parent, Quantity, How>::GetQuantity()
    {
      return quantity_;
    }

    template<class Parent, class Quantity, class How> 
    inline const Physic::Quantity& 
    SensorT<Parent, Quantity, How>::GetQuantity() const
    {
      return quantity_;
    }


    template<class Parent, class Quantity, class How> 
    inline void 
    SensorT<Parent, Quantity, How>::DrawShape()
    {
      quantity_.Draw();
    }

    template<class Parent, class Quantity, class How> 
    inline void 
    SensorT<Parent, Quantity, How>::BeginDraw()
    {
      quantity_.BeginDraw();
    }

    template<class Parent, class Quantity, class How> 
    inline void 
    SensorT<Parent, Quantity, How>::EndDraw()
    {
      quantity_.EndDraw();
    }


    template<class Parent, class Quantity, class How>
    inline void 
    SensorT<Parent, Quantity, How>::Acquire() 
    { 
      quantity_.GetRealDataType().Update(How::Acquire(*this));
    }

    template<class Parent, class Quantity, class How>
    inline void 
    SensorT<Parent, Quantity, How>::SaveState()
    {
      Acquire();
    }

    template<class Parent, class Quantity, class How>
    inline void 
    SensorT<Parent, Quantity, How>::ResetState()
    {
      quantity_.GetDataType().ResetState();
    }

    template<class Parent, class Quantity, class How>
    inline void 
    SensorT<Parent, Quantity, How>::RecoveryState(unsigned int i)
    {
      quantity_.GetDataType().RecoveryState(i);
    }

    template<class Parent, class Quantity, class How>
    inline const Geom::Frame<_3D>& 
    SensorT<Parent, Quantity, How>::GetFrame() const
    {
      return GetParent().GetFrame();
    }

  

    template<class Parent, class Quantity, class How>
    inline const Core::AutoRegisteredPtr<Parent, SensorT<Parent, Quantity, How> >& 
    SensorT<Parent, Quantity, How>::GetParentItemPtr() const
    {
      return parentItem_;
    }

    
    template<class Parent, class Quantity, class How>
    inline Core::AutoRegisteredPtr<Parent, SensorT<Parent, Quantity, How> >& 
    SensorT<Parent, Quantity, How>::GetParentItemPtr()
      {
      return parentItem_;
    }


    template<class Parent, class Quantity, class How>
    inline Parent& 
    SensorT<Parent, Quantity, How>::GetParent()
    {
      return *GetParentItemPtr().GetPtr();
    }
    
    template<class Parent, class Quantity, class How>
    inline const Parent& 
    SensorT<Parent, Quantity, How>::GetParent() const
    {
      return *GetParentItemPtr().GetPtr();      
    }


  }
} 


namespace boost 
{ 
  namespace serialization 
  {
    
    template<class Archive, class Parent, class Quantity, class How>
    inline void save_construct_data(Archive & ar, 
				    const OpenMeca::Item::SensorT<Parent, Quantity, How> * t, 
				    const unsigned int)
    {
      const Parent* parent = &t->GetParent();
      ar << parent;
    }
    

    template<class Archive, class Parent, class Quantity, class How>
    inline void load_construct_data(Archive & ar, 
				    OpenMeca::Item::SensorT<Parent, Quantity, How> * t, 
				    const unsigned int)
    {
      Parent* parent = 0;
      ar >> parent;
      ::new(t)OpenMeca::Item::SensorT<Parent, Quantity, How>(*parent);
    }
  }
} // namespace ...


#endif
