// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/PartUser.hpp"
#include "OpenMeca/Item/Body.hpp"

#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(OpenMeca::Item::PartUser)

namespace OpenMeca
{  
  namespace Item
  {
     
    const std::string 
    PartUser::GetStrType()
    {
      return std::string("PartUser");
    }



    
    PartUser::PartUser(const std::string strType, Core::UserItem& parent)
      :Part(strType, parent.GetMainTreeItem(), true),
       parentItem_(*this, parent),
       center_(boost::bind(&PartUser::GetReferenceFrame, boost::ref(*this))),
       quaternion_(boost::bind(&PartUser::GetReferenceFrame, boost::ref(*this))),
       frame_(center_, quaternion_),
       bodyIsDeleted_(false)
    {
    }

  
    
    
    PartUser::~PartUser()
    {
    }



    
    Core::UserItem&
    PartUser::GetParentItem()
    {
      return *parentItem_.GetPtr();
    }


    const Core::UserItem&
    PartUser::GetParentItem() const
    {
      return *parentItem_.GetPtr();
    }
  


    
     const Geom::Frame<_3D>&
    PartUser::GetReferenceFrame() const
    {
      return GetParentItem().GetFrame();
    }

    
    QTreeWidgetItem& 
    PartUser::GetParentTreeItem()
    {
      return GetParentItem().GetMainTreeItem();
    }


    
     const Util::Color& 
    PartUser::GetColor() const
    {
      return GetParentItem().GetColor();
    }
    
    
     Util::Color& 
    PartUser::GetColor()
    {
      return GetParentItem().GetColor();
    }


    
    const Core::AutoRegisteredPtr<Core::UserItem, PartUser >& 
    PartUser::GetParentItemPtr() const
    {
      return parentItem_;
    }

    
    
    Core::AutoRegisteredPtr<Core::UserItem, PartUser >& 
    PartUser::GetParentItemPtr()
    {
      return parentItem_;
    }


    const Body& 
    PartUser::GetBody() const
    {
      return GetParentItem().GetBody();
    }

    Body& 
    PartUser::GetBody()
    {
      return GetParentItem().GetBody();
    }

    void 
    PartUser::BodyIsDeleted()
    {
      bodyIsDeleted_ = true;
    }



  }
}




