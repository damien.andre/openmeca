// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Part_hpp
#define OpenMeca_Item_Part_hpp

#include <boost/lexical_cast.hpp>
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/DrawableUserItem.hpp"



namespace OpenMeca
{  

  namespace Item
  {

    class Body;
    
    // Base class for all parts. A part is a drawable entity registered
    // in the treeview.
    class Part: public Core::DrawableUserItem, public Core::AutoRegister<Part>
    {
    
    public:
      static const std::string GetStrType(); 
      static const QString GetQStrType(); 
      static double GetScaleValue();

    public:
      Part(const std::string strType, QTreeWidgetItem& parent, bool isSelectable);
      virtual ~Part();
      
      virtual Geom::Point<_3D>& GetCenter(); // forbidden
      virtual const Geom::Point<_3D>& GetCenter() const; // forbidden 
      virtual Geom::Quaternion<_3D>& GetQuaternion(); // forbidden
      virtual const Geom::Quaternion<_3D>& GetQuaternion() const ; // forbidden

    private:
      Part(); //Not allowed, just for serialization
      Part(const Part&);             //Not Allowed
      Part& operator=(const Part&);  //Not Allowed

      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive& ar, const unsigned int);
    };

    template<class Archive>
    void Part::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Core::DrawableUserItem);
    }
    

  }

}




#endif
