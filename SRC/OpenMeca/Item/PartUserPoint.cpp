// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "OpenMeca/Item/PartUserPoint.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/UserRootItemCommonProperty.hpp"

#include "OpenMeca/Item/PartPoint_CreateAction_SpecializedT.hpp"

#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(OpenMeca::Item::PartUserPoint)

namespace OpenMeca
{  
  namespace Core
  {

    template<>
    void 
    UserItemCommonProperty<OpenMeca::Item::PartUserPoint>::CreateAction_Specialized() 
    {
      AddPopUpSeparator();
      
      // We call PartPoint_CreateAction_SpecializedT because it is needed by other class
      UserItemCommonProperty<OpenMeca::Item::PartUserPoint>& me = *this;
      OpenMeca::Item::PartPoint_CreateAction_SpecializedT(me);
    }

  }
}


namespace OpenMeca
{  
  namespace Item
  {


    void
    PartUserPoint::Init()
    {
      Core::UserItemCommonProperty< PartUserPoint >& prop = 
	Core::Singleton< Core::UserItemCommonProperty< PartUserPoint > >::Get();

      prop.CreateAction_Edit();
      prop.CreateAction_Delete();
      
      // don't forget to instanciate UserRootItemCommonProperty<Body> before calling
      // create action
      Core::Singleton< Core::UserRootItemCommonProperty<OpenMeca::Item::Body> >::Instanciate();
      prop.CreateAction_NewWithAutomaticSelection<OpenMeca::Item::Body>();
     
      prop.CreateAction_Specialized();
    }


    void 
    PartUserPoint::DrawIcon(QIcon& icon, QColor color)
    {
      PartPoint::DrawIcon(icon, color);
    }

    
    PartUserPoint::PartUserPoint(Core::UserItem& parent)
      :PartPoint(GetStrType(), parent),
       centerExpr_(boost::bind(&PartUser::GetReferenceFrame, boost::ref(*this))),
       visible_(false)
    {
    }

    PartUserPoint::~PartUserPoint()
    {
    }

    void 
    PartUserPoint::Update()
    {
      PartPoint::Update();
      GetPoint() = centerExpr_;
    }

    void 
    PartUserPoint::PostDraw()
    {
      if (visible_)
	{
	  glColor3f(1.0f, 1.0f, 1.0f);
	  const Geom::Point<_3D>& p = PartUser::GetPoint();
	  const qglviewer::Vec v1(p[0], p[1], p[2]);
	  const qglviewer::Vec v2 = Gui::MainWindow::Get().GetViewer().camera()->projectedCoordinatesOf(v1);
	  const int x = v2.x;
	  const int y = v2.y;
	  Gui::MainWindow::Get().GetViewer().drawText(x, y - 4, GetName().c_str());
	}
    }

  }
}




