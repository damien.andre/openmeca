// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Shape_ShapeBase_hpp
#define OpenMeca_Item_Shape_ShapeBase_hpp

#include <QString>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include "OpenMeca/Item/PartUser.hpp"

namespace OpenMeca
{
  namespace Item
  {
    namespace Shape
    {


      class ShapeBase
      {
      public:
	ShapeBase(PartUser& part);
	virtual ~ShapeBase();
	
	virtual void Draw() const = 0;
	virtual void BeginDraw() = 0;


	virtual void BuildChSystem(chrono::ChSystem&);

	// Accessors
	OMC_ACCESSOR(Part, PartUser,  part_);
	
      private:
	friend class boost::serialization::access;
	template<class Archive> void serialize(Archive& ar, const unsigned int version);
	
      private:
	PartUser& part_;
      }; 
    

      template<class Archive>
      inline void
      ShapeBase::serialize(Archive& ar, const unsigned int)
      {
      }

      

    }
  }
}


#endif
