 // This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/PartUserJunction.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Item/PartPoint.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/UserItemCommonProperty.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Util/Draw.hpp"


#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(OpenMeca::Item::PartUserJunction)



namespace OpenMeca
{  
  namespace Item
  {


    void
    PartUserJunction::Init()
    {
      Core::Singleton< Core::UserItemCommonProperty< PartUserJunction > >::Get().CreateAction_Edit();
      Core::Singleton< Core::UserItemCommonProperty< PartUserJunction > >::Get().CreateAction_Delete();
    }



    void 
    PartUserJunction::DrawIcon(QIcon& icon, QColor color)
    {
      Util::Icon::DrawIconFromSvgFile(":/Rsc/Img/Part/Junction.svg", icon, color);
    }


    
    PartUserJunction::PartUserJunction(PartPoint& parent)
      :PartUser(GetStrType(), parent),
       startPoint_(parent),
       length_(0),
       endPoint_(*this)
    {

      // find the end points, must be different from the start points
      Core::SetOfBase<PartPoint> set = GetBody().GetChildSet<PartPoint>();
      for (unsigned int i = 0; i < set.GetTotItemNumber();++i)
	{
	if (&set(i) != & startPoint_)
	  {
	    endPoint_ = &set(i);
	    break;
	  }
	}
    }

  
    
    
    PartUserJunction::~PartUserJunction()
    {
    }

	     
    void 
    PartUserJunction::BeginDraw()
    {
      if (endPoint_.GetPtr() == 0)
	return;

      //Translate
      Geom::Point<_3D>& p1 = PartUser::GetPoint();
      p1 = Geom::Point<_3D>(GetStartPoint().GetPoint(),
			   boost::bind(&PartUser::GetReferenceFrame, boost::ref(*this)));

      //Rotate
      Geom::Point<_3D> p2(GetEndPoint().GetPoint(), 
			   boost::bind(&PartUser::GetReferenceFrame, boost::ref(*this)));
      Geom::Vector<_3D> axis(p1, p2);
      Geom::Quaternion<_3D>& q = PartUser::GetQuaternion();
      const Geom::Vector<_3D>& X = GetReferenceFrame().GetXAxis();
      q.SetVecFromTo(X,axis);
      length_ = axis.GetNorm();
    }

    void 
    PartUserJunction::DrawShape()
    {
      const double scale = Part::GetScaleValue();
      Util::Draw::Cylinder(.01f*scale, length_, false);
    }

    void 
    PartUserJunction::UpdateIcon()
    {
      PartUserJunction::DrawIcon(GetIcon(), GetColor().GetQColor());
    }
    
    PartPoint& 
    PartUserJunction::GetEndPoint()
    {
      OMC_ASSERT_MSG(endPoint_.GetPtr() != 0, "The end point is null");
      return *endPoint_.GetPtr();
    }

    const PartPoint& 
    PartUserJunction::GetEndPoint() const
    {
      OMC_ASSERT_MSG(endPoint_.GetPtr() != 0, "The end point is null");
      return *endPoint_.GetPtr();
    } 


    Core::AutoRegisteredPtr<PartPoint, PartUserJunction >& 
    PartUserJunction::GetEndPointPtr()
    {
      return endPoint_;
    }
    
    const Core::AutoRegisteredPtr<PartPoint, PartUserJunction >& 
    PartUserJunction::GetEndPointPtr() const
    {
      return endPoint_;      
    }
    

  }
}


namespace OpenMeca
{  
  namespace Core
  {

    template<>
    void
    ItemCommonProperty<OpenMeca::Item::PartUserJunction>::BuildIconSymbol()
    {
      QIcon icon;
      OpenMeca::Item::PartUserJunction::DrawIcon(iconSymbol_, Qt::gray);
    }



  }
}


