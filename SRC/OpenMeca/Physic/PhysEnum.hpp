// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Physic_PhysEnum_hpp
#define OpenMeca_Physic_PhysEnum_hpp


namespace OpenMeca
{
  namespace Physic
  {    

    class MechActionEnum
    {
    public:
      enum DirectionMode {ABSOLUTE_DIR = FDIR_WORLD, BODY_COORDINATE_DIR=FDIR_BODY};
      enum StartPointMode{ABSOLUTE_POS = FPOS_WORLD, BODY_COORDINATE_POS=FPOS_BODY};
    }; 


  }
}


#endif
