// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QWidget>

#include "OpenMeca/Physic/Force.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Core/System.hpp"

#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::Force)

namespace OpenMeca
{  
  namespace Physic
  {

    const std::string
    Force::GetStrType()
    {
      return "Force";
    }

    const QString
    Force::GetQStrType()
    {
      return QObject::tr("Force");
    }
      

    Force::Force(Item::Physical& item)
      :MechanicalAction(item, Util::Color::Red),
       unit_(Util::Dimension::Get("Force").GetUnit("Newton"))
    {
      x_.SetDimension(Util::Dimension::Get("Force"));
      y_.SetDimension(Util::Dimension::Get("Force"));
      z_.SetDimension(Util::Dimension::Get("Force"));
    }
   
    Force::~Force()
    {
    }

 
    const Util::Unit& 
    Force::GetUnit() const
    {
      return unit_;
    }
    
    double
    Force::GetScale() const
    {
      return Core::System::Get().GetScales().GetScaleValue(Force::GetStrType());
    }


    void 
    Force::CompleteChForce(chrono::ChSharedForcePtr& chForce)
    {
      chForce->SetMode(FTYPE_FORCE);
    }

  }
}
