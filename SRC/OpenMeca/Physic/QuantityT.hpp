// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Physic_QuantityT_hpp
#define OpenMeca_Physic_QuantityT_hpp

#include "OpenMeca/Physic/Quantity.hpp"
#include "OpenMeca/Physic/Vector3D.hpp"

namespace OpenMeca
{
  namespace Physic
  {    

    template<class T>
    class QuantityT : public Quantity
    {
    public:
      QuantityT(Item::Physical&, const Util::Color& color);
      virtual ~QuantityT();	

      Type& GetDataType();
      const Type& GetDataType() const;

      T& GetRealDataType();
      const T& GetRealDataType() const;

      virtual void Draw();
      virtual void BeginDraw();
      virtual void EndDraw();
      
    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    protected:
      T data_;     

    }; 

    template<class T>
    template<class Archive>
    inline void
    QuantityT<T>::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Quantity);
      ar & BOOST_SERIALIZATION_NVP(data_);
    }

    template<class T>
    inline 
    QuantityT<T>::QuantityT(Item::Physical& item, const Util::Color& color)
      :Quantity(item, color), data_(*this)
    {
    }

    template<class T>
    inline 
    QuantityT<T>::~QuantityT()
    {
    }

    template<class T>
    inline Type&
    QuantityT<T>::GetDataType()
    {
      return data_;
    }

    template<class T>
    inline const Type&
    QuantityT<T>::GetDataType() const
    {
      return data_;
    }

    template<class T>
    inline T&
    QuantityT<T>::GetRealDataType()
    {
      return data_;
    }

    template<class T>
    inline const T&
    QuantityT<T>::GetRealDataType() const
    {
      return data_;
    }

    template<class T>
    inline void 
    QuantityT<T>::Draw()
    {
      Quantity::Draw();
    }

    template<class T>
    inline void 
    QuantityT<T>::BeginDraw()
    {
      Quantity::BeginDraw();
    }

    template<class T>
    inline void 
    QuantityT<T>::EndDraw()
    {
      Quantity::EndDraw();
    }

    





    template<> void QuantityT<Vector3D>::Draw();
    template<> void QuantityT<Vector3D>::BeginDraw();
    template<> void QuantityT<Vector3D>::EndDraw();




  }
}


#endif
