// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef _OpenMeca_Physic_TypeT_hpp_
#define _OpenMeca_Physic_TypeT_hpp_

#include "OpenMeca/Physic/Type.hpp"

namespace OpenMeca
{
  namespace Physic
  {

    template<class T>
    class TypeT : public Type
    {
    public:
      TypeT(Quantity&);
      virtual ~TypeT();

      virtual T& GetRealType() = 0;
      virtual const T& GetRealType() const = 0;
      
      virtual void Update(const T&) = 0;

    private:
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int);
      
    };

    
    template<class T>
    template<class Archive>
    inline void
    TypeT<T>::serialize(Archive & ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Type);
    }


    template<class T>
    inline 
    TypeT<T>::TypeT(Quantity& quantity)
      :Type(quantity)
    {
    }


    template<class T>
    inline 
    TypeT<T>::~TypeT()
    {
    }


  }
} 

#endif
