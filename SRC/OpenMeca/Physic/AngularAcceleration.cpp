// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QWidget>

#include "OpenMeca/Physic/AngularAcceleration.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Core/System.hpp"

#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::AngularAcceleration)

namespace OpenMeca
{  
  namespace Physic
  {

    const Util::Color AngularAcceleration::color = Util::Color::Green;
    
    const std::string
    AngularAcceleration::GetStrType()
    {
      return "AngularAcceleration";
    }

    const QString
    AngularAcceleration::GetQStrType()
    {
      return QObject::tr("AngularAcceleration");
    }
      

    AngularAcceleration::AngularAcceleration(Item::Physical& item)
      :QuantityT<Vector3D>(item, color),
       unit_(Util::Dimension::Get("AngularAcceleration").GetUnit("RadianPerSecondSquared"))
    {
      
    }
   
    AngularAcceleration::~AngularAcceleration()
    {
    }

 
    const Util::Unit& 
    AngularAcceleration::GetUnit() const
    {
      return unit_;
    }
    
    double
    AngularAcceleration::GetScale() const
    {
      return Core::System::Get().GetScales().GetScaleValue(AngularAcceleration::GetStrType());
    }



  }
}
