// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef _OpenMeca_Physic_Vector3D_hpp_
#define _OpenMeca_Physic_Vector3D_hpp_

#include <QObject>
#include <QMenu>
#include <QTreeWidget>

#include "OpenMeca/Physic/TypeT.hpp"
#include "OpenMeca/Physic/Quantity.hpp"
#include "OpenMeca/Geom/Vector.hpp"
#include "OpenMeca/Util/Unit.hpp"
#include "OpenMeca/Util/CustomChFunction.hpp"

namespace OpenMeca
{
  namespace Physic
  {

    class Vector3D : public QObject, public TypeT <Geom::Vector<_3D> >
    {
      Q_OBJECT
      
    public:
      Vector3D(Quantity&);
      ~Vector3D();

      virtual void Update(const Geom::Vector<_3D>&);
      virtual void Update();

      virtual void SaveState();
      virtual void ResetState();
      void RecoveryState(unsigned int);
      Geom::Vector<_3D>& GetRealType();
      const Geom::Vector<_3D>& GetRealType() const;
			       
      void FillDataTree(QTreeWidgetItem* tree);
      void UpdateDataTree();
      
      void WriteHeaderDataFile(std::ofstream&);
      void WriteDataFile(std::ofstream&, unsigned int);	

      void Draw() const;

      void SetCustomChFunction(Util::CustomChFunction*, Util::CustomChFunction*, Util::CustomChFunction*);
      void UpdateValueWithChFunction();
						      
    private slots:
      void X(bool);
      void Y(bool);
      void Z(bool);
      void Norm(bool);

    private:
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int);
    
    private:
      Geom::Vector<_3D> vec_;
      std::vector<double> x_;
      std::vector<double> y_;
      std::vector<double> z_;
      std::vector<double> norm_;
      Util::CustomChFunction* xfunc_;
      Util::CustomChFunction* yfunc_;
      Util::CustomChFunction* zfunc_;
      QTreeWidgetItem* xitem_;
      QTreeWidgetItem* yitem_;
      QTreeWidgetItem* zitem_;
      QTreeWidgetItem* nitem_;
    };


    template<class Archive>
    inline void
    Vector3D::serialize(Archive & ar, const unsigned int)
    {
	ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(TypeT <Geom::Vector<_3D> >);
	ar & BOOST_SERIALIZATION_NVP(vec_);
    }

  }
} 

namespace boost 
{ 
  namespace serialization 
  {
    
    template<class Archive>
    inline void save_construct_data(Archive & ar, 
				    const OpenMeca::Physic::Vector3D * t, 
				    const unsigned int)
    {
      const OpenMeca::Physic::Quantity* parent = &t->GetQuantity();
      ar << parent;
    }
    

    template<class Archive>
    inline void load_construct_data(Archive & ar, 
				    OpenMeca::Physic::Vector3D * t, 
				    const unsigned int)
    {
      OpenMeca::Physic::Quantity* parent = 0;
      ar >> parent;
      ::new(t)OpenMeca::Physic::Vector3D(*parent);
    }
  }
} // namespace ...


#endif
