// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QMenu>
#include <QMessageBox>

#include "qwt_legend.h"

#include "OpenMeca/Gui/GeneralSetting.hpp"
#include "OpenMeca/Core/ConfigDirectory.hpp"
#include "OpenMeca/Setting/LangManager.hpp"
#include "OpenMeca/Util/Lang.hpp"
#include "OpenMeca/Core/ConfigDirectory.hpp"

namespace OpenMeca
{
  namespace Gui
  {
  

    GeneralSetting::GeneralSetting(QWidget * parent)
      :QDialog(parent)
    {
      setupUi(this);
      ReadLang();

      QObject::connect(buttonBox_, SIGNAL(accepted()), this, SLOT(Apply()));
      QObject::connect(pushButtonReset_, SIGNAL(clicked()), this, SLOT(Reset()));
    }
    
    GeneralSetting::~GeneralSetting()
    {
    }
    

    void
    GeneralSetting::ReadLang()
    {
      const Util::Lang& current = Setting::LangManager::Get().GetUserChoice();
      std::map<const std::string, Util::Lang*>& lang = Util::Lang::GetAll();
      std::map<const std::string, Util::Lang*>::iterator it;

      for (it = lang.begin(); it != lang.end(); ++it)
	comboBoxLang_->addItem(it->second->GetName().c_str());
      comboBoxLang_->setCurrentIndex(current.GetIndex());
    }
  
    void
    GeneralSetting::Apply()
    {
      ApplyLang();
      NotifyRestart();
      
    }

    void
    GeneralSetting::NotifyRestart()
    {
      QMessageBox::information(this, tr("Information"),
			       tr("You must restart openmeca to apply settings"));
    }

    void
    GeneralSetting::ApplyLang()
    {
      const Util::Lang& lang = Util::Lang::GetByIndex(comboBoxLang_->currentIndex());
      Setting::LangManager::Get().SetUserChoice(lang);
    }

    void
    GeneralSetting::Reset()
    {
      QString msg;
      msg += QObject::tr("You are going to reset the setting of openmeca, are you sure ?");
      QMessageBox mbox(QMessageBox::Information, tr("Question"), msg,
		       QMessageBox::Yes|QMessageBox::No); 
      if (QMessageBox::Yes == mbox.exec())
	{
	  Core::ConfigFile::RestoreAll();
	  NotifyRestart();
	  hide();
	}
    }
    
  }
}
