// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Widget_WidgetSelectItemT_hpp
#define OpenMeca_Widget_WidgetSelectItemT_hpp


#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QComboBox>


#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/Item.hpp"
#include "OpenMeca/Gui/Widget/WidgetSelectItem.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    template<class T>
    class WidgetSelectItemT: public WidgetSelectItem
    {
      
    public:
      WidgetSelectItemT(QWidget* parent);
      virtual ~WidgetSelectItemT();

      void ApplyValue();
      typename T::PtrType& GetSelectedItem();
      void SetSelectedItem(typename T::PtrType&);

      void SetList(Core::SetOfBase<typename T::PtrType>& set);

    private:
      void UpdateComboBox();
      typename T::PtrType& GetItemAtIndex(int i);
      int GetIndexForItem(typename T::PtrType& item);
      void ItemSelected_CallBack(Core::Item&);


    private:
      std::map<unsigned int, typename T::PtrType*> map_;
      Core::SetOfBase<typename T::PtrType> set_;
    };


    template<class T>
    inline
    WidgetSelectItemT<T>::WidgetSelectItemT(QWidget* parent)
      :WidgetSelectItem(parent),
       map_(),
       set_()
    {
    
    }
    

    template<class T>
    inline
    WidgetSelectItemT<T>::~WidgetSelectItemT()
    {
    }

    template<class T>
    inline void 
    WidgetSelectItemT<T>::SetList(Core::SetOfBase<typename T::PtrType>& set)
    {
      set_ = set;
      UpdateComboBox();
    }


    template<class T>
    inline typename T::PtrType& 
    WidgetSelectItemT<T>::GetItemAtIndex(int i)
    {
      OMC_ASSERT_MSG(map_.count(i)==1, "There is not any item registered at this index");
      return *map_[i];
    }

    template<class T>
    inline int
    WidgetSelectItemT<T>::GetIndexForItem(typename T::PtrType& item)
    {
      typename std::map<unsigned int, typename T::PtrType*>::iterator it;
      for ( it=map_.begin() ; it != map_.end(); it++ )
	{
	  if ((*it).second == &item)
	    return (*it).first;
	}
      OMC_ASSERT_MSG(0, "Can't find item");
      return -1;
    }
    

    template<class T>
    inline void 
    WidgetSelectItemT<T>::UpdateComboBox()
    {
      WidgetSelectItem::ClearComboBox();
      typename std::list<typename T::PtrType*>::iterator it;
      unsigned int index = 0;
      map_.clear();
      for (it=set_.Begin() ; it != set_.End(); it++ )
	{
	  map_[index] = *it;
	  const typename T::PtrType& item = **it;
	  WidgetSelectItem::GetComboBox().addItem(item.GetIcon(), item.GetName().c_str());
	  index ++;
	}
      
    }
    

    template<class T>
    inline void 
    WidgetSelectItemT<T>::SetSelectedItem(typename T::PtrType& ptr)
    {
      UpdateComboBox();
      const int currentIndex = GetIndexForItem(ptr);
      WidgetSelectItem::GetComboBox().setCurrentIndex(currentIndex);
    }


    template<class T>
    inline typename T::PtrType&
    WidgetSelectItemT<T>::GetSelectedItem()
    {
      return GetItemAtIndex(WidgetSelectItem::GetComboBox().currentIndex());
    }

    template<class T>
    inline void 
    WidgetSelectItemT<T>::ItemSelected_CallBack(Core::Item& item)
    {
      if (typeid(typename T::PtrType)==typeid(item))
	{
	  typename T::PtrType& cast = static_cast<typename T::PtrType&>(item);
	  WidgetSelectItem::GetComboBox().setCurrentIndex(GetIndexForItem(cast));
	}
    }

  }
}
#endif

