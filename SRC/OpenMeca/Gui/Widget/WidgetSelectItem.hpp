// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_Widget_WidgetSelectItem_hpp
#define OpenMeca_Gui_Widget_WidgetSelectItem_hpp


#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QComboBox>

#include "OpenMeca/Core/Item.hpp"
#include "ui_WidgetSelectItem.h"

namespace OpenMeca
{
  namespace Gui
  {
    class WidgetSelectItem :  public QWidget,
			      private Ui::WidgetSelectItem
    {
      Q_OBJECT

      public:
      WidgetSelectItem(QWidget* parent);
      virtual ~WidgetSelectItem();
      QComboBox& GetComboBox();							     

    protected:
      virtual void ItemSelected_CallBack(Core::Item&) = 0;
      void ClearComboBox();
      
      virtual void hideEvent(QHideEvent *event);
      virtual void showEvent(QShowEvent *event);

    public slots:
      void ItemSelected(Core::Item&);

    };

  }
}
#endif

