// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QToolTip>
#include <iostream>

#include "OpenMeca/Gui/Widget/WidgetDouble.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Util/Unit.hpp"
#include "OpenMeca/Core/Macro.hpp"

namespace OpenMeca
{
  namespace Gui
  {

    WidgetDouble::WidgetDouble(QWidget* parent)
      :QWidget(parent),
       dim_(0)
    {
      Ui::WidgetDouble::setupUi(this);
    }

    WidgetDouble::~WidgetDouble()
    {
      for (unsigned int i = 0; i < condition_.size(); ++i)
	delete condition_[i];
    }

    
    void 
    WidgetDouble::SetDimension(const Util::Dimension& dim)
    {
      dim_ = &dim;
      unitLabel_->setText(dim_->GetUserChoice().GetSymbol().c_str());
    }

    const Util::Dimension&
    WidgetDouble::GetDimension() const
    {
      OMC_ASSERT_MSG(dim_!=0, "The widget double has no dimension");
      return *dim_;
    }

    void 
    WidgetDouble::SetNumber(double value)
    {
      double val = value/dim_->GetUserChoice().GetFactor();
      GetLineEdit().setText(QString::number(val));
    }
   
    bool
    WidgetDouble::GetValue(double& val)
    {
      bool conversionOk = false;
      double curVal = lineEdit_->text().toDouble(&conversionOk);
      if (conversionOk==false)
	{
	  DisplayHelp("This is not a number");
	}
      else
	{
	  conversionOk = CheckCondition(curVal);
	  if (conversionOk)
	    {
	      lineEdit_->setPalette(QApplication::palette());
	      val = curVal * dim_->GetUserChoice().GetFactor();
	    }
	}
      return conversionOk;
    }

    QLineEdit&
    WidgetDouble::GetLineEdit()
    {
      return *lineEdit_;
    }

    void 
    WidgetDouble::DisplayHelp(const QString& msg)
    {
      QToolTip::showText(mapToGlobal( QPoint( 0, 0 ) ), msg);
    }

    void 
    WidgetDouble::AddCondition(const Core::Condition<double>* cond)
    {
      condition_.push_back(cond);
    }

    bool
    WidgetDouble::CheckCondition(const double& val)
    {
      for (unsigned int i = 0; i < condition_.size(); ++i)
	{
	  if (condition_[i]->Check(val) == false)
	    {
	      DisplayHelp(condition_[i]->ErrorMessage());
	      return false;
	    }
	}
      return true;
    }

    QString 
    WidgetDouble::GetInputString() const
    {
      return lineEdit_->text();
    }


  }
}

