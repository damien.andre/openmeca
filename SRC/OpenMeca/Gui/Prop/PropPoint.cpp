// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QApplication>
#include <iostream>

#include "OpenMeca/Gui/Prop/PropPoint.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Util/Unit.hpp"

namespace OpenMeca
{
  namespace Gui
  {

    PropPoint::PropPoint(QWidget* parent)
      :PropT<Geom::Point<_3D> >(*parent),
       itemX_(),
       itemY_(),
       itemZ_(),
       x_(0),
       y_(0),
       z_(0)
    {
      AddSubItem(&itemX_);
      AddSubItem(&itemY_);
      AddSubItem(&itemZ_);
    }

    PropPoint::~PropPoint()
    {
    }

    
    void 
    PropPoint::Insert(PropTree& tree)
    {
      tree.addTopLevelItem(&item_);
      item_.setText(0, Prop::GetLabel());
      
      item_.addChild(&itemX_);
      itemX_.setText(0, "X");
      tree.setItemWidget(&itemX_,1, &x_);

      item_.addChild(&itemY_);
      itemY_.setText(0, "Y");
      tree.setItemWidget(&itemY_,1, &y_);

      item_.addChild(&itemZ_);
      itemZ_.setText(0, "Z");
      tree.setItemWidget(&itemZ_,1, &z_);

    }

    void 
    PropPoint::SetDimension(const Util::Dimension& dim)
    {
      x_.SetDimension(dim);
      y_.SetDimension(dim);
      z_.SetDimension(dim);
    }



    void 
    PropPoint::Init()
    {
      const Geom::Point<_3D>& p =  PropT<Geom::Point<_3D> >::GetValue();    
      x_.SetNumber(p[0]);
      y_.SetNumber(p[1]);
      z_.SetNumber(p[2]);
    }
    

   
    bool
    PropPoint::Check()
    {
      double x, y, z;
      bool ok = (x_.GetValue(x) && y_.GetValue(y) && z_.GetValue(z));
      if (ok)
	{
	  Geom::Point<_3D> &p = PropT<Geom::Point<_3D> >::GetCopy();
	  p[0] = x;
	  p[1] = y;
	  p[2] = z;
	}
      return ok;
    }

    void 
    PropPoint::AddComponentCondition(const Core::Condition<double>* cond)
    {
      x_.AddCondition(cond);
      y_.AddCondition(cond->Copy());
      z_.AddCondition(cond->Copy());
    }

  }
}


