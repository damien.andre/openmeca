// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QApplication>
#include <iostream>

#include "OpenMeca/Gui/Prop/PropTree.hpp"
#include "OpenMeca/Gui/Prop/PropString.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    PropString::PropString(QWidget* parent)
      :PropT<std::string>(*parent),
       lineEdit_()
    {
      lineEdit_.setAttribute(Qt::WA_TranslucentBackground);
    }

    PropString::~PropString()
    {
    }

    void
    PropString::Insert(PropTree& tree)
    {
      tree.addTopLevelItem(&item_);
      item_.setText(0, Prop::GetLabel());
      tree.setItemWidget(&item_,1, &lineEdit_);
    }

    void 
    PropString::Init()
    {
      const std::string value =  PropT<std::string>::GetValue();
      lineEdit_.setText(value.c_str());
    }
    
    bool
    PropString::Check()
    {
      if (lineEdit_.text().isEmpty())
	{
	  QPalette p = lineEdit_.palette();
	  p.setColor(QPalette::Base, Qt::red);
	  lineEdit_.setPalette(p);
	  DisplayHelp("The text is empty !");
	  return false;
	}
      else
	{
	  lineEdit_.setPalette(QApplication::palette());
	  PropT<std::string>::GetCopy() = lineEdit_.text().toStdString();
	}
      return true;
    }

     
    void 
    PropString::DisplayHelp(const QString& msg)
    {
      QToolTip::showText(lineEdit_.mapToGlobal(QPoint(0,0)), msg);
    }



  }
}

