// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QApplication>
#include <iostream>

#include "OpenMeca/Gui/Prop/PropBool.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Util/Unit.hpp"

namespace OpenMeca
{
  namespace Gui
  {

    PropBool::PropBool(QWidget* parent)
      :PropT<bool>(*parent),
       checkBox_()
    {
      checkBox_.setAttribute(Qt::WA_TranslucentBackground);
    }
    
    PropBool::~PropBool()
    {
    }

    void
    PropBool::Insert(PropTree& tree)
    {
      tree.addTopLevelItem(&item_);
      item_.setText(0, GetLabel());
      tree.setItemWidget(&item_,1, &checkBox_);
    }


    void 
    PropBool::Init()
    {
      const bool value =  PropT<bool>::GetValue();
      if (value)
	checkBox_.setCheckState(Qt::Checked);
      else
	checkBox_.setCheckState(Qt::Unchecked);
    }
   
    bool
    PropBool::Check()
    {
      if (checkBox_.checkState() == Qt::Checked)
	PropT<bool>::GetCopy() = true;
      else if (checkBox_.checkState() == Qt::Unchecked)
	PropT<bool>::GetCopy() = false;
      else
	OMC_ASSERT_MSG(0, "This case is forbidden");

      return true;
    }

  }
}
