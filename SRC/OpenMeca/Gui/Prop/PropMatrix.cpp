// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QApplication>
#include <iostream>

#include "OpenMeca/Gui/Prop/PropMatrix.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Util/Unit.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    PropMatrix::PropMatrix(QWidget* parent)
      :PropT<Geom::Matrix<_3D> >(*parent),
       itemXX_(),
       itemXY_(),
       itemXZ_(),
       itemYX_(),
       itemYY_(),
       itemYZ_(),
       itemZX_(),
       itemZY_(),
       itemZZ_(),
       xx_(0),
       yx_(0),
       zx_(0),
       xy_(0),
       yy_(0),
       zy_(0),
       xz_(0),
       yz_(0),
       zz_(0)
    {
      QObject::connect(&xy_.GetLineEdit(), SIGNAL(textChanged(const QString&)), 
		       this               , SLOT(XYEdited(const QString&))); 
      QObject::connect(&xz_.GetLineEdit(), SIGNAL(textChanged(const QString&)), 
		       this               , SLOT(XZEdited(const QString&))); 
      QObject::connect(&yz_.GetLineEdit(), SIGNAL(textChanged(const QString&)), 
		       this               , SLOT(YZEdited(const QString&))); 

      AddSubItem(&itemXX_);
      AddSubItem(&itemXY_);
      AddSubItem(&itemXZ_);
      AddSubItem(&itemYX_);
      AddSubItem(&itemYY_);
      AddSubItem(&itemYZ_);
      AddSubItem(&itemZX_);
      AddSubItem(&itemZY_);
      AddSubItem(&itemZZ_);
    }

    PropMatrix::~PropMatrix()
    {
    }

    void 
    PropMatrix::Insert(PropTree& tree)
    {
      tree.addTopLevelItem(&item_);
      item_.setText(0, Prop::GetLabel());
      
      // X
      item_.addChild(&itemXX_);
      itemXX_.setText(0, "XX");
      tree.setItemWidget(&itemXX_,1, &xx_);

      item_.addChild(&itemXY_);
      itemXY_.setText(0, "XY");
      tree.setItemWidget(&itemXY_,1, &xy_);

      item_.addChild(&itemXZ_);
      itemXZ_.setText(0, "XZ");
      tree.setItemWidget(&itemXZ_,1, &xz_);

      // Y
      item_.addChild(&itemYX_);
      itemYX_.setText(0, "YX");
      tree.setItemWidget(&itemYX_,1, &yx_);

      item_.addChild(&itemYY_);
      itemYY_.setText(0, "YY");
      tree.setItemWidget(&itemYY_,1, &yy_);

      item_.addChild(&itemYZ_);
      itemYZ_.setText(0, "YZ");
      tree.setItemWidget(&itemYZ_,1, &yz_);

      // Z
      item_.addChild(&itemZX_);
      itemZX_.setText(0, "ZX");
      tree.setItemWidget(&itemZX_,1, &zx_);

      item_.addChild(&itemZY_);
      itemZY_.setText(0, "ZY");
      tree.setItemWidget(&itemZY_,1, &zy_);

      item_.addChild(&itemZZ_);
      itemZZ_.setText(0, "ZZ");
      tree.setItemWidget(&itemZZ_,1, &zz_);

    }

    
    void 
    PropMatrix::SetDimension(const Util::Dimension& dim)
    {
      xx_.SetDimension(dim);
      xy_.SetDimension(dim);
      xz_.SetDimension(dim);

      yx_.SetDimension(dim);
      yy_.SetDimension(dim);
      yz_.SetDimension(dim);

      zx_.SetDimension(dim);
      zy_.SetDimension(dim);
      zz_.SetDimension(dim);
    }


  

    void 
    PropMatrix::Init()
    {
      const Geom::Matrix<_3D>& m =  PropT<Geom::Matrix<_3D> >::GetValue();    
      xx_.SetNumber(m[0][0]);
      xy_.SetNumber(m[0][1]);
      xz_.SetNumber(m[0][2]);

      yx_.SetNumber(m[1][0]);
      yy_.SetNumber(m[1][1]);
      yz_.SetNumber(m[1][2]);

      zx_.SetNumber(m[2][0]);
      zy_.SetNumber(m[2][1]);
      zz_.SetNumber(m[2][2]);

    }
    
   
    bool
    PropMatrix::Check()
    {
      double xx, xy, xz, yx, yy, yz, zx, zy, zz;
      bool ok = (xx_.GetValue(xx) && xy_.GetValue(xy) && xz_.GetValue(xz) &&
		 yx_.GetValue(yx) && yy_.GetValue(yy) && yz_.GetValue(yz) &&
		 zx_.GetValue(zx) && zy_.GetValue(zy) && zz_.GetValue(zz));
       if (ok)
	{
	  Geom::Matrix<_3D> &m = PropT<Geom::Matrix<_3D> >::GetCopy();
	  m[0][0] = xx;
	  m[0][1] = xy;
	  m[0][2] = xz;
	  
	  m[1][0] = yx;
	  m[1][1] = yy;
	  m[1][2] = yz;
	  
	  m[2][0] = zx;
	  m[2][1] = zy;
	  m[2][2] = zz;

	}
      return ok;
    
    }

    void
    PropMatrix::XYEdited(const QString& str)
    {
      yx_.GetLineEdit().setText(str);
    }

    void
    PropMatrix::XZEdited(const QString& str)
    {
      zx_.GetLineEdit().setText(str);
    }

    void
    PropMatrix::YZEdited(const QString& str)
    {
      zy_.GetLineEdit().setText(str);
    }

       
    void 
    PropMatrix::AddComponentCondition(const Core::Condition<double>* cond)
    {
      xx_.AddCondition(cond);
      xy_.AddCondition(cond->Copy());
      xz_.AddCondition(cond->Copy());

      yx_.AddCondition(cond->Copy());
      yy_.AddCondition(cond->Copy());
      yz_.AddCondition(cond->Copy());

      zx_.AddCondition(cond->Copy());
      zy_.AddCondition(cond->Copy());
      zz_.AddCondition(cond->Copy());
    }


  }
}

