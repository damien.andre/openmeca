// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QApplication>
#include <iostream>

#include "OpenMeca/Gui/Prop/PropDouble.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"
#include "OpenMeca/Util/Unit.hpp"

namespace OpenMeca
{
  namespace Gui
  {

    PropDouble::PropDouble(QWidget* parent)
      :PropT<double>(*parent),
       widgetDouble_(0)
    {
    }

    PropDouble::~PropDouble()
    {
    }

    void 
    PropDouble::Insert(PropTree& tree)
    {
      tree.addTopLevelItem(&item_);
      item_.setText(0, Prop::GetLabel());
      tree.setItemWidget(&item_,1, &widgetDouble_);
    }
    
    void 
    PropDouble::SetDimension(const Util::Dimension& dim)
    {
      widgetDouble_.SetDimension(dim);
    }

    const Util::Dimension&
    PropDouble::GetDimension() const
    {
      return widgetDouble_.GetDimension();
    }

    void 
    PropDouble::Init()
    {
      const double& value =  PropT<double>::GetValue();
      widgetDouble_.SetNumber(value);
    }
   
    bool
    PropDouble::Check()
    {
      double& value = PropT<double>::GetCopy();
      return (widgetDouble_.GetValue(value));
    }

    void 
    PropDouble::DisplayHelp(const QString& message)
    {
      widgetDouble_.DisplayHelp(message);
    }

    void 
    PropDouble::AddCondition(const Core::Condition<double>* cond)
    {
      widgetDouble_.AddCondition(cond);
    }

    QString 
    PropDouble::GetInputString() const
    {
      return widgetDouble_.GetInputString();
    }

  }
}


