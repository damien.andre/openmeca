// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QApplication>
#include <iostream>

#include "OpenMeca/Gui/Prop/PropExprDouble.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Util/Unit.hpp"

namespace OpenMeca
{
  namespace Gui
  {

    PropExprDouble::PropExprDouble(QWidget* parent)
      :PropT<Util::ExprDouble>(*parent),
       w_item_(),
       val_(0)
    {
      AddSubItem(&w_item_);
    }

    PropExprDouble::~PropExprDouble()
    {
    }

    
    void 
    PropExprDouble::Insert(PropTree& tree)
    {
      tree.addTopLevelItem(&item_);
      item_.setText(0, Prop::GetLabel());
      
      item_.addChild(&w_item_);
      w_item_.setText(0, "X");
      tree.setItemWidget(&w_item_,1, &val_);

    }

    void 
    PropExprDouble::SetDimension(const Util::Dimension& dim)
    {
      val_.SetDimension(dim);
    }

    const Util::Dimension& 
    PropExprDouble::GetDimension() const
    {
      return val_.GetDimension();
    }

    void 
    PropExprDouble::Init()
    {
      Util::ExprDouble& p = PropT<Util::ExprDouble >::GetValue();    
      val_.SetExpr(p.GetExpression());
    }
    

   
    bool
    PropExprDouble::Check()
    {
      Util::ExprDouble& p = PropT<Util::ExprDouble >::GetCopy();
      Util::Expr& x = p.GetExpression();
      return (val_.GetExpr(x));
    }

    void 
    PropExprDouble::AddComponentCondition(const Core::Condition<double>* cond)
    {
      val_.AddCondition(cond);
    }

    void     
    PropExprDouble::PostChangement()
    {
      Util::ExprDouble& p = PropT<Util::ExprDouble >::GetValue();
      p.GetExpression().Update();
    }


  }
}


