// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Prop_PropSelectItemT_hpp
#define OpenMeca_Prop_PropSelectItemT_hpp


#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QComboBox>


#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/UserItem.hpp"
#include "OpenMeca/Gui/Widget/WidgetSelectItemT.hpp"
#include "OpenMeca/Gui/Prop/PropT.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    template<class T>
    class PropSelectItemT: public PropT<T>
    {
      
    public:
      PropSelectItemT(QWidget* parent);
      virtual ~PropSelectItemT();

      void Insert(PropTree&);

      bool Check();
      void ApplyValue();
      void SetList(Core::SetOfBase<typename T::PtrType>& set);

      typename T::PtrType& GetSelectedItem();

    private:
      void Init();

    private:
      WidgetSelectItemT<T> widgetSelect_;

    };


    template<class T>
    inline
    PropSelectItemT<T>::PropSelectItemT(QWidget* parent)
      :PropT<T>(*parent),
       widgetSelect_(parent)
    {
    
    }
    

    template<class T>
    inline
    PropSelectItemT<T>::~PropSelectItemT()
    {
    }

    template<class T>
    inline void
    PropSelectItemT<T>::Insert(PropTree& tree)
    {
      QTreeWidgetItem* item = &PropT<T>::GetTreeWidgetItem();
      tree.addTopLevelItem(item);
      item->setText(0, Prop::GetLabel());
      tree.setItemWidget(item,1, &widgetSelect_);
    }

  

    template<class T>
    inline void 
    PropSelectItemT<T>::Init()
    {
      widgetSelect_.SetSelectedItem(*PropT<T>::GetCopy().GetPtr());
    }
    

   
    template<class T>
    inline bool
    PropSelectItemT<T>::Check()
    {
      PropT<T>::GetCopy() = &widgetSelect_.GetSelectedItem();
      return true;
    }


    template<class T>
    inline void 
    PropSelectItemT<T>::SetList(Core::SetOfBase<typename T::PtrType>& set)
    {
      widgetSelect_.SetList(set);
    }

    template<class T>
    inline  typename T::PtrType&
    PropSelectItemT<T>::GetSelectedItem()
    {
      return widgetSelect_.GetSelectedItem();
    }

  }
}
#endif

