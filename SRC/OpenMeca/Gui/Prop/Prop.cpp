// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>


#include "OpenMeca/Gui/Prop/Prop.hpp"
#include "OpenMeca/Gui/Dialog/Dialog.hpp"

namespace OpenMeca
{
  namespace Gui
  {
  
    Prop::Prop(QWidget& parent)
      : item_(),
	subItem_(),
	color_(Qt::white),
	label_(""),
	parent_(0),
	parentDeleted_(false)
    {
      bool castOk = false;
      QObject* widget = &parent;
      do
	{
	  if (CouldCastToDialog(widget)==true)
	    {
	      parent_ = static_cast<Dialog*>(widget);
	      castOk = true;
	      break;
	    }
	  OMC_ASSERT_MSG(widget != 0, "The widget pointer is null");
	  widget = widget->parent();
	} while (castOk == false);
      
      GetParent().AddProp(*this);

      QFont font("" , 9 , QFont::Bold );
      item_.setFont( 0,  font );

      QBrush b(QColor(100,100,100));
      item_.setForeground(0, b);
    }
  
    Prop::~Prop()
    {
      if (parentDeleted_==false)
	GetParent().RemoveProp(*this);
    }

    bool
    Prop::CouldCastToDialog(QObject* widget)
    {
      Dialog* test = 0;
      test = dynamic_cast<Dialog*>(widget);	  
      return (test!=0);
    }

    Dialog& 
    Prop::GetParent()
    {
      OMC_ASSERT_MSG(parent_!=0, "The parent pointer is null");
      OMC_ASSERT_MSG(parentDeleted_ == false, "The parent is deleted");
      return *parent_;
    }

    void
    Prop::ParentDeleted()
    {
      parentDeleted_ = true;
    }

    void 
    Prop::SetLabel(const QString& str)
    {
      label_ = str;
    }

    QString 
    Prop::GetLabel() const
    {
      return label_;
    }

    QTreeWidgetItem& 
    Prop::GetTreeWidgetItem()
    {
      return item_;
    }

    void 
    Prop::SetColor(const QColor& c)
    {
      color_ = c;
      QBrush b(color_);
      item_.setBackground(0, b);
      item_.setBackground(1, b);
      for (unsigned int i = 0; i < subItem_.size(); ++i)
	{
	  subItem_[i]->setBackground(0, b);
	  subItem_[i]->setBackground(1, b);
	}
    }

    void
    Prop::AddSubItem(QTreeWidgetItem* item)
    {
      subItem_.push_back(item);
    }

    void
    Prop::DisplayHelp(const std::string&)
    {
      OMC_ASSERT_MSG(0, "Can't call this method on base class !");
    }

  }
}
