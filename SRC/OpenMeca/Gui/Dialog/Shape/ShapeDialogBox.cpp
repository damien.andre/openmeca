// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QWidget>

#include "OpenMeca/Item/Shape/Box.hpp"
#include "OpenMeca/Gui/Dialog/Shape/ShapeDialogBox.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    namespace Shape
    {
    
      ShapeDialogBox::ShapeDialogBox(QWidget* widget):
	lx_(widget),
	ly_(widget),
	lz_(widget),
	attitude_(widget)
      {
	lx_.SetDimension(Util::Dimension::Get("Length"));
	lx_.SetLabel(QObject::tr("Length along X"));
	lx_.AddCondition(new Core::Superior<double>(0.));
	
	ly_.SetDimension(Util::Dimension::Get("Length"));
	ly_.SetLabel(QObject::tr("Length along Y"));
	ly_.AddCondition(new Core::Superior<double>(0.));

	lz_.SetDimension(Util::Dimension::Get("Length"));
	lz_.SetLabel(QObject::tr("Length along Z"));
	lz_.AddCondition(new Core::Superior<double>(0.));

	attitude_.SetLabel(QObject::tr("Rotation"));
      }
      
      void 
      ShapeDialogBox::Add(Gui::PropTree& prop)
      {
	prop.Add(lx_);
	prop.Add(ly_);
	prop.Add(lz_);
	prop.Add(attitude_);
      }
      
      void 
      ShapeDialogBox::Init(OpenMeca::Item::Shape::Box& s)
      {
	lx_.SetValue(s.GetLengthX());
	ly_.SetValue(s.GetLengthY());
	lz_.SetValue(s.GetLengthZ());
	attitude_.SetValue(s.GetAttitude());
      }
    
    }
  }
}
