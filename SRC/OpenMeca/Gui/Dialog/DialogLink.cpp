// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QSpacerItem>
#include <QMessageBox>

#include "OpenMeca/Gui/Dialog/DialogLink.hpp"
#include "OpenMeca/Gui/Dialog/DialogUserItem.hpp"
#include "OpenMeca/Item/Link.hpp"
#include "OpenMeca/Item/Body.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    
    DialogLink::DialogLink(DialogUserItem* me)
      :me_(*me),
       id_(me),
       body1_(me),
       body2_(me),
       point_(me),
       quaternion_(me),
       drawLocalFrame_(me)
    { 
      id_.SetLabel(DialogUserItem::tr("Name"));
      body1_.Prop::SetLabel(DialogUserItem::tr("First body"));
      body2_.Prop::SetLabel(DialogUserItem::tr("Second body"));

      point_.Prop::SetLabel(DialogUserItem::tr("Center"));
      point_.SetDimension(Util::Dimension::Get("Length"));
      
      quaternion_.SetLabel(DialogUserItem::tr("Rotation"));

      drawLocalFrame_.Prop::SetLabel(DialogUserItem::tr("Draw local frame"));
      
      // Init table
      me_.GetPropTree().Add(id_);
      me_.GetPropTree().Add(body1_);
      me_.GetPropTree().Add(body2_);
      me_.GetPropTree().Add(point_);
      me_.GetPropTree().Add(quaternion_);
      me_.GetPropTree().Add(drawLocalFrame_);
    }
    
    DialogLink::~DialogLink()
    {
    }

    void 
    DialogLink::Init()
    {
      id_.SetValue(GetCurrentLink().GetName());

      body1_.SetList(Core::AutoRegister<Item::Body>::GetGlobalSet());
      body2_.SetList(Core::AutoRegister<Item::Body>::GetGlobalSet());
      body1_.SetValue(GetCurrentLink().GetBody1Ptr());
      body2_.SetValue(GetCurrentLink().GetBody2Ptr());

      point_.SetValue(GetCurrentLink().GetCenter());
      
      quaternion_.SetValue(GetCurrentLink().GetQuaternion());
     
      drawLocalFrame_.SetValue(GetCurrentLink().DrawLocalFrame());

    }
    

    bool
    DialogLink::Check()
    {
      if (&body1_.GetSelectedItem() == &body2_.GetSelectedItem())
	{
	  QMessageBox::warning(&me_, QObject::tr("Warning"), 
			       QObject::tr("The bodys must be different"));
	  return false;
	}
      return true;
    }

    PropSelectItemT<Core::AutoRegisteredPtr<Item::Body, Item::Link> >& 
    DialogLink::GetPropSelectItemBody1()
    {
      return body1_;
    }
    
    PropSelectItemT<Core::AutoRegisteredPtr<Item::Body, Item::Link> >&
    DialogLink::GetPropSelectItemBody2()
    {
      return body2_;
    }

    
  
  }
}
