// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Gui/Dialog/DialogLinkPulley.hpp"
#include "OpenMeca/Item/Link/Pulley.hpp"
#include "OpenMeca/Item/LinkT.hpp"


namespace OpenMeca
{
  namespace Gui
  {

  
    DialogLinkPulley::DialogLinkPulley()
      :DialogLinkT<Item::LinkT<Item::Pulley> >(),
       radius1_(this),
       radius2_(this),
       interAxisLength_(this)
    {
      radius1_.SetLabel(QObject::tr("Pinion 1 radius"));
      radius1_.SetDimension(Util::Dimension::Get("Length"));

      radius2_.SetLabel(QObject::tr("Pinion 2 radius"));
      radius2_.SetDimension(Util::Dimension::Get("Length"));

      interAxisLength_.SetLabel(QObject::tr("Length between axes"));
      interAxisLength_.SetDimension(Util::Dimension::Get("Length"));

      GetPropTree().Add(radius1_);
      GetPropTree().Add(radius2_);
      GetPropTree().Add(interAxisLength_);
    }

    
    DialogLinkPulley::~DialogLinkPulley()
    {
    }

    void
    DialogLinkPulley::Init()
    {
      DialogLinkT<Item::LinkT<Item::Pulley> >::Init();
      radius1_.SetValue(GetCurrentItem().GetLinkType().GetRadius1());
      radius2_.SetValue(GetCurrentItem().GetLinkType().GetRadius2());
      interAxisLength_.SetValue(GetCurrentItem().GetLinkType().GetInterAxisLength());
    }


    bool
    DialogLinkPulley::Check()
    {      
      return DialogLinkT<Item::LinkT<Item::Pulley> >::Check();
    }


  }
}
