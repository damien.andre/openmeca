// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.



#include "OpenMeca/Gui/Dialog/DialogBody.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Util/Dimension.hpp"

namespace OpenMeca
{
  namespace Gui
  {
  
    DialogBody::DialogBody()
      :DialogUserItemT<Item::Body>(),
       id_(this),
       color_(this),
       ground_(this),
       showReferenceFrame_(this),
       showMassCenter_(this),
       mass_(this),
       massCenter_(this),
       inertia_(this),
       staticFrictionCoef_(this),
       slidingFrictionCoef_(this)
    {
      id_.SetLabel(QObject::tr("Name"));
      
      color_.SetLabel(QObject::tr("Color"));
      
      ground_.SetLabel(QObject::tr("Clamped"));
      showReferenceFrame_.SetLabel(QObject::tr("Show local frame"));
      showMassCenter_.SetLabel(QObject::tr("Show mass center"));
      
      mass_.SetDimension(Util::Dimension::Get("Mass"));
      mass_.SetLabel(QObject::tr("Mass"));
      mass_.AddCondition(new Core::Superior<double>(0.));
      
      massCenter_.SetLabel(QObject::tr("Center"));
      massCenter_.SetDimension(Util::Dimension::Get("Length"));
      
      inertia_.SetDimension(Util::Dimension::Get("Inertia"));
      inertia_.SetLabel(QObject::tr("Inertia"));
      inertia_.AddComponentCondition(new Core::Superior<double>(0.));
      
      staticFrictionCoef_.SetLabel(QObject::tr("Static friction coeficient"));
      staticFrictionCoef_.SetDimension(Util::Dimension::Get("Null"));

      slidingFrictionCoef_.SetLabel(QObject::tr("Sliding friction coeficient"));
      slidingFrictionCoef_.SetDimension(Util::Dimension::Get("Null"));

      // Init table
      GetPropTree().Add(id_);
      GetPropTree().Add(color_);
      GetPropTree().Add(ground_);
      GetPropTree().Add(showReferenceFrame_);
      GetPropTree().Add(showMassCenter_);
      GetPropTree().Add(mass_);
      GetPropTree().Add(massCenter_);
      GetPropTree().Add(inertia_);
      GetPropTree().Add(staticFrictionCoef_);
      GetPropTree().Add(slidingFrictionCoef_);

    }
  
    DialogBody::~DialogBody()
    {
    }

    void
    DialogBody::Init()
    {
       id_.SetValue(GetCurrentItem().GetName());
       color_.SetValue(GetCurrentItem().GetColor());
       ground_.SetValue(GetCurrentItem().GetFixed());
       showReferenceFrame_.SetValue(GetCurrentItem().IsReferenceFrameShown());
       showMassCenter_.SetValue(GetCurrentItem().IsMassCenterShown());
       mass_.SetValue(GetCurrentItem().GetMass());
       massCenter_.SetValue(GetCurrentItem().GetMassCenter());
       inertia_.SetValue(GetCurrentItem().GetInertia());
       staticFrictionCoef_.SetValue(GetCurrentItem().GetStaticFrictionCoef());
       slidingFrictionCoef_.SetValue(GetCurrentItem().GetSlidingFrictionCoef());

    }

    void
    DialogBody::ApplyChangement()
    {
      DialogUserItemT<Item::Body>::ApplyChangement();
      GetCurrentItem().InitAndManageState();      
    }

   

  
  }
}
