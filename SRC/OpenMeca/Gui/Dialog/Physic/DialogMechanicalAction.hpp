// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_Physic_DialogMechanicalAction_hpp
#define OpenMeca_Gui_Physic_DialogMechanicalAction_hpp

#include <QWidget>
#include "OpenMeca/Gui/Prop/PropEnumT.hpp"
#include "OpenMeca/Gui/Prop/PropVector.hpp"
#include "OpenMeca/Physic/PhysEnum.hpp"
#include "OpenMeca/Gui/Prop/PropExpr.hpp"

namespace OpenMeca
{ 
  namespace Physic
  { 
    class MechanicalAction; 
  }
}

namespace OpenMeca
{
  namespace Gui
  {
    namespace Physic
    {
      class DialogMechanicalAction
      {
      public:
	DialogMechanicalAction(QWidget* widget);
	virtual ~DialogMechanicalAction();
	virtual void Add(Gui::PropTree& prop);
	virtual void Init(OpenMeca::Physic::MechanicalAction& f);
	
      protected:
	PropExpr x_;
	PropExpr y_;
	PropExpr z_;
	PropEnumT<OpenMeca::Physic::MechActionEnum::DirectionMode> direction_;
      };

      
    }
  }
}
#endif
