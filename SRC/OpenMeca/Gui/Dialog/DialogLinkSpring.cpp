// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Gui/Dialog/DialogLinkSpring.hpp"
#include "OpenMeca/Item/Link/Spring.hpp"
#include "OpenMeca/Item/LinkT.hpp"




namespace OpenMeca
{
  namespace Gui
  {

  
    DialogLinkSpring::DialogLinkSpring()
      :DialogLinkT<Item::LinkT<Item::Spring> >(),
       stiffness_(this),
       dampingFactor_(this),
       freeLength_(this),
       initialLength_(this)
    {
      stiffness_.SetLabel(QObject::tr("Stiffness"));
      stiffness_.SetDimension(Util::Dimension::Get("Stiffness"));

      dampingFactor_.SetLabel(QObject::tr("Damping factor"));
      dampingFactor_.SetDimension(Util::Dimension::Get("DampingFactor"));

      dampingFactor_.SetLabel(QObject::tr("Damping factor"));
      dampingFactor_.SetDimension(Util::Dimension::Get("DampingFactor"));

      freeLength_.Prop::SetLabel(QObject::tr("Free length"));
      freeLength_.SetDimension(Util::Dimension::Get("Length"));

      initialLength_.Prop::SetLabel(QObject::tr("Initial length"));
      initialLength_.SetDimension(Util::Dimension::Get("Length"));

      GetPropTree().Add(stiffness_);
      GetPropTree().Add(dampingFactor_);
      GetPropTree().Add(freeLength_);
      GetPropTree().Add(initialLength_);
    }

    
    DialogLinkSpring::~DialogLinkSpring()
    {
    }

    void
    DialogLinkSpring::Init()
    {
      DialogLinkT<Item::LinkT<Item::Spring> >::Init();
      stiffness_.SetValue(GetCurrentItem().GetLinkType().GetStiffness());
      dampingFactor_.SetValue(GetCurrentItem().GetLinkType().GetDampingFactor());
      freeLength_.SetValue(GetCurrentItem().GetLinkType().GetFreeLength());
      initialLength_.SetValue(GetCurrentItem().GetLinkType().GetInitialLength());
    }

    bool
    DialogLinkSpring::Check()
    {      
      return DialogLinkT<Item::LinkT<Item::Spring> >::Check();
    }


  }
}
