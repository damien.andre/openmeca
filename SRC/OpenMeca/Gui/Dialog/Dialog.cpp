// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <iostream>
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Gui/Dialog/Dialog.hpp"
#include "OpenMeca/Gui/Prop/Prop.hpp"

namespace OpenMeca
{
  namespace Gui
  {

  
    Dialog::Dialog()
      :QWidget(&MainWindow::Get().GetDialogContainer()),
       widgets_(),
       action_(0)
    {
     
    }

    
    Dialog::~Dialog()
    {
      Core::SetOf<Prop>::it it;
      for (it = widgets_.Begin() ; it != widgets_.End(); it++ )
	(*it)->ParentDeleted();
    }

    void 
    Dialog::Show_CallBack()
    {
      MainWindow::Get().DialogContainerIsBusy(*this);
    }

    void 
    Dialog::Hide_CallBack()
    {
      MainWindow::Get().DialogContainerIsFree(*this);
    }

    void 
    Dialog::AddProp(Prop& w)
    {
      widgets_.AddItem(w);
    }

    bool 
    Dialog::Check()
    {
      Core::SetOf<Prop>::it it;
      for (it = widgets_.Begin() ; it != widgets_.End(); it++ )
	if ((*it)->Check()==false)
	  return false;
      return true;
    }

    

    void
    Dialog::ApplyChangement()
    {
      Core::SetOf<Prop>::it it;
      for (it = widgets_.Begin() ; it != widgets_.End(); it++ )
	(*it)->ApplyChangement();      
    }

    void
    Dialog::CancelChangement()
    {
      Core::SetOf<Prop>::it it;
      for (it = widgets_.Begin() ; it != widgets_.End(); it++ )
	(*it)->CancelChangement();      
    }


    void 
    Dialog::SetAction(QAction& action)
    {
      action_ = &action;
    }
    
    QAction&
    Dialog::GetAction()
    {
      OMC_ASSERT_MSG(action_ != 0, "The action is null");
      return *action_;
    }

    void 
    Dialog::Reset()
    {
      action_ = 0;
      Core::SetOf<Prop>::it it;
      for (it = widgets_.Begin() ; it != widgets_.End(); it++ )
	(*it)->Reset();
    }

    void 
    Dialog::AddActionToHistoric()
    {
      MainWindow::Get().AddToHistoric(GetAction());
      
    }

    bool
    Dialog::IsPossibleToClose()
    {
      return true;
    }


  }
}
