// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_UnitManager_hpp
#define OpenMeca_Core_UnitManager_hpp

#include <QDomDocument>
#include <QMenu>

#include "OpenMeca/Core/GlobalSettingT.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Util/Dimension.hpp"

namespace OpenMeca
{
  namespace Setting
  {

    // The MenuManager manage the file that customize the openmeca's units
    class UnitManager : public Core::GlobalSettingT<UnitManager>  
    {
      friend class Core::Singleton<UnitManager> ;

    public:  
      static void Init();
      static const std::string GetStrType(); 
  
      std::string GetClassId() const;
      void ReadXmlFile();
      void WriteXmlFile();
      
      typedef double Dialog; // fake, change this plz !

    private:
      UnitManager();
      ~UnitManager();

      void AddDimension(QDomElement&);
      void AddUnit(QDomElement&, Util::Dimension&);
      
    private:
      
    private :
      UnitManager(const UnitManager&);             //Not Allowed
      UnitManager& operator=(const UnitManager&);  //Not Allowed
    }; 
    
    

  }
}
#endif
