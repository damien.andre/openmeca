// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_LangManager_hpp
#define OpenMeca_Core_LangManager_hpp

#include <QDomDocument>
#include <QMenu>

#include "OpenMeca/Core/GlobalSettingT.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Util/Lang.hpp"

namespace OpenMeca
{
  namespace Setting
  {

    // The MenuManager manage the file that customize the openmeca's units
    class LangManager : public Core::GlobalSettingT<LangManager>  
    {
      friend class Core::Singleton<LangManager> ;

    public:  
      static void Init();
      static const std::string GetStrType(); 
  
      void ReadXmlFile();
      void WriteXmlFile();

      void AddLang(QDomElement& e);
      void SetUserChoice(QDomElement& e);
      void SetUserChoice(const Util::Lang& lang);
      void Apply(QApplication& app);
      const Util::Lang& GetUserChoice();
      
      typedef double Dialog; // fake, change this plz !

    private:
      LangManager();
      ~LangManager();

    private :
      LangManager(const LangManager&);             //Not Allowed
      LangManager& operator=(const LangManager&);  //Not Allowed


    private:
      Util::Lang* userChoice_;


    }; 
    
    

  }
}
#endif
