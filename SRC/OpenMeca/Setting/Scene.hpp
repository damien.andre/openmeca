// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Setting_Scene_hpp
#define OpenMeca_Setting_Scene_hpp

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "OpenMeca/Core/SystemSettingT.hpp"
#include "OpenMeca/Core/SetOf.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/None.hpp"
#include "OpenMeca/Core/Macro.hpp"

#include "OpenMeca/Geom/Point.hpp"
#include "OpenMeca/Util/Color.hpp"

#include "QGLViewer/camera.h"

namespace OpenMeca
{
  namespace Gui
  {
    class DialogScene;
  }

  namespace Setting
  {

    class Scene : public Core::SystemSettingT<Scene>, public Core::AutoRegister<Scene>
    {
    public:
      typedef Gui::DialogScene Dialog;

    public:  
      static void Init();
      static const std::string GetStrType();
      static const QString GetQStrType();

    public:    
      Scene();
      ~Scene();




      void Update();
      void ComputeOptimalSceneRadius();

      void Save(boost::archive::text_oarchive&, const unsigned int);
      void Load(boost::archive::text_iarchive&, const unsigned int);

      // Accessor
      OMC_ACCESSOR(DrawAxis       , bool                  , drawAxis_       );
      OMC_ACCESSOR(BackgroundColor, Util::Color           , backgroundColor_);
      OMC_ACCESSOR(SceneCenter    , Geom::Vector<_3D>     , sceneCenter_    );
      OMC_ACCESSOR(SceneRadius    , double                , sceneRadius_    );
      OMC_ACCESSOR(CameraType    , qglviewer::Camera::Type, cameraType_    );


    private:
      Scene(const Scene&);            //Not Allowed    
      Scene& operator=(const Scene&); //Not Allowed

    private:
      bool drawAxis_;
      Util::Color backgroundColor_;
      Geom::Vector<_3D> sceneCenter_;
      double sceneRadius_;
      qglviewer::Camera::Type cameraType_;
      
      
    };

   
  }
}
 
#endif
