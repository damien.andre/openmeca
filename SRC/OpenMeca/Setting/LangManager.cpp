// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <boost/lexical_cast.hpp>

#include "OpenMeca/Setting/LangManager.hpp"
#include "OpenMeca/Core/GlobalSettingCommonProperty.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Util/Unit.hpp"

#include <QTranslator>

namespace OpenMeca
{
  namespace Setting
  {

    void
    LangManager::Init()
    {
      Core::Singleton< Core::GlobalSettingCommonProperty<LangManager> >::Get();
      Core::Singleton<LangManager>::Get().ReadXmlFile();
    }

    const std::string
    LangManager::GetStrType()
    {
      return "LangManager";
    }

    

    LangManager::LangManager()
      :Core::GlobalSettingT<LangManager>(),
       userChoice_(0)
    {
      
    }
    
   
    LangManager::~LangManager()
    {
    }

    void LangManager::ReadXmlFile()
    {
      QDomDocument doc("");
      QFile& file = OpenXmlConfigFile();
      OMC_ASSERT_MSG(doc.setContent(&file), "Can't set content of the xml doc");
      QDomNode n = doc.firstChild();
      while (!n.isNull()) 
	{
	  if (n.isElement()) 
	    {
	      QDomElement e = n.toElement();
	      if (e.tagName()==QString(GetStrType().c_str()))
		{
		  QDomNode n1 = e.firstChild ();
		  while (!n1.isNull()) 
		    {
		      if (n1.isElement()) 
			{
			  QDomElement e1(n1.toElement());
			  if (e1.tagName() == "Lang")
			    AddLang(e1);
			  else if (e1.tagName() == "App")
			    SetUserChoice(e1);
			  else
			    OMC_ASSERT_MSG(0, "Unknown case");
			}
		      n1 = n1.nextSibling();
		    }
		}
	    }
	  n = n.nextSibling();
	}
    }
    
 
    void LangManager::AddLang(QDomElement& e)
    {
      const std::string id   = e.attribute ("ID").toStdString();
      const std::string name = e.attribute ("Name").toStdString();
      const std::string file = e.attribute ("File").toStdString();

      OMC_ASSERT_MSG(!id.empty() && !name.empty(), "One of these attributes is empty");
      new Util::Lang(id, name, file);
      
    }

    void LangManager::SetUserChoice(QDomElement& e)
    {
      OMC_ASSERT_MSG(userChoice_ == 0, "User choice must be null before its initialization");
      
      userChoice_ = &Util::Lang::GetByID(e.attribute ("UserChoice").toStdString());
    }

    void LangManager::WriteXmlFile()
    {
      OMC_ASSERT_MSG(0, "TODO");
    }

    const Util::Lang& 
    LangManager::GetUserChoice()
    {
      OMC_ASSERT_MSG(userChoice_ != 0, "The user choice is null");
      return *userChoice_;
    }

    void 
    LangManager::Apply(QApplication& app)
    {
      OMC_ASSERT_MSG(userChoice_ != 0, "The user choice is null");
      const QString file = userChoice_->GetFile().c_str();      
      if (file != "")
	{
	  QTranslator* tr = new QTranslator();
	  tr->load(":/Rsc/Lang/" + file);
	  app.installTranslator(tr);
	}
    }

    void 
    LangManager::SetUserChoice(const Util::Lang& lang)
    {
      QDomDocument doc("");
      QFile& file = OpenXmlConfigFile();
      OMC_ASSERT_MSG(doc.setContent(&file), "Can't set doc content");
      QDomNode n = doc.firstChild();
      while (!n.isNull()) 
	{
	  if (n.isElement()) 
	    {
	      QDomElement e = n.toElement();
	      if (e.tagName()==QString(GetStrType().c_str()))
		{
		  QDomNode n1 = e.firstChild ();
		  while (!n1.isNull()) 
		    {
		      if (n1.isElement()) 
			{
			  QDomElement e1(n1.toElement());
			  if (e1.tagName() == "App")
			    {
			      e1.setAttribute("UserChoice", lang.GetID().c_str());
			    }
			}
		      n1 = n1.nextSibling();
		    }
		}
	    }
	  n = n.nextSibling();
	}
      
      SaveXmlConfigFile(doc);
      

    }

  }
}
