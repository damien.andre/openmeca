// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Setting/Scales.hpp"
#include "OpenMeca/Item/Part.hpp"
#include "OpenMeca/Core/SystemSettingCommonProperty.hpp"
#include "OpenMeca/Gui/Dialog/DialogScales.hpp"

#include "OpenMeca/Physic/LinearVelocity.hpp"
#include "OpenMeca/Physic/LinearAcceleration.hpp"
#include "OpenMeca/Physic/AngularVelocity.hpp"
#include "OpenMeca/Physic/AngularAcceleration.hpp"
#include "OpenMeca/Physic/Force.hpp"
#include "OpenMeca/Physic/Torque.hpp"

namespace OpenMeca
{
  namespace Setting
  {

    const 
    std::string Scales::GetStrType()
    {
      return "Scales";
    }

    const 
    QString Scales::GetQStrType()
    {
      return QObject::tr("Scales");
    }


    void
    Scales::Init()
    {
      Core::SystemSettingT< Scales>::Get();      
      Core::Singleton< Core::SystemSettingCommonProperty<Scales> >::Get().RegisterAction();
    }


    Scales::Scales()
      :Core::SystemSettingT<Scales>(),
       scales_(),
       widget_(0)
    {

      // set all the scales
      AddScale(OpenMeca::Item::Part::GetStrType()); 
      AddScale(OpenMeca::Geom::Frame<_3D>::GetStrType()); 
      AddScale(OpenMeca::Physic::LinearVelocity::GetStrType()); 
      AddScale(OpenMeca::Physic::LinearAcceleration::GetStrType()); 
      AddScale(OpenMeca::Physic::AngularVelocity::GetStrType()); 
      AddScale(OpenMeca::Physic::AngularAcceleration::GetStrType()); 
      AddScale(OpenMeca::Physic::Force::GetStrType()); 
      AddScale(OpenMeca::Physic::Torque::GetStrType()); 
    }

    Scales::~Scales()
    {
    }

    void 
    Scales::SetWidget(Gui::WidgetScales& widget)
    {
      widget_ = &widget;
    }
    
     void 
     Scales::AddScale(const std::string& scaleKey)
     {
       OMC_ASSERT_MSG(scales_.count(scaleKey) == 0, "This scale already exist");
       scales_[scaleKey] = 1.;
       scaleKeys_.push_back(scaleKey);
     } 

    double& 
    Scales::GetScaleValue(const std::string& scaleKey)
    {
      OMC_ASSERT_MSG(scales_.count(scaleKey) == 1, "Can't find this scale");
      return scales_[scaleKey];
    }

    const std::vector<std::string>& 
    Scales::GetKeys() const 
    {
      return scaleKeys_;
    }

    void
    Scales::Update()
    {
      OMC_ASSERT_MSG(widget_ != 0, "Widget is null");
      widget_->Update();
    }

    void 
    Scales::Save(boost::archive::text_oarchive& ar, const unsigned int )
    {
      const unsigned int size = scales_.size();
      ar << BOOST_SERIALIZATION_NVP(size);

      std::map<std::string, double>::const_iterator it;
      for (it=scales_.begin(); it!=scales_.end(); ++it)
	{
	  const std::string key = it->first;
	  const double value = it->second;
	  ar << BOOST_SERIALIZATION_NVP(key);
	  ar << BOOST_SERIALIZATION_NVP(value);
	}
    }

    void 
    Scales::Load(boost::archive::text_iarchive& ar, const unsigned int ) 
    {
      unsigned int size = 0;
      ar >> BOOST_SERIALIZATION_NVP(size);
      for (unsigned int i=0; i<size; i++)
	{
	  std::string key = "";
	  double value = 0;
	  ar >> BOOST_SERIALIZATION_NVP(key);
	  ar >> BOOST_SERIALIZATION_NVP(value);
	  OMC_ASSERT_MSG(key != "", "The key is empty");
	  scales_[key] = value;
	}
    }

  }
}
 

