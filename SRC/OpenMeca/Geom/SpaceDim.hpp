// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


// This source file was inspired of the "libGeometrical" from 
// the GranOO workbench : http://www.granoo.org


#ifndef _OpenMeca_Geom_SpaceDim1_H
#define _OpenMeca_Geom_SpaceDim1_H

#include <string>

namespace OpenMeca
{
  namespace Geom 
  {
    
    enum SpaceDim
      {
	_0D =0,
	_1D =1,
	_2D =2,
	_3D =3
      };

    template<SpaceDim N>
    struct SpaceDimUtil
    {
      static std::string GetStrKey();
    };
    
  }
}

#endif
